#### 1. bpf_probe_read_kernel

```
long bpf_probe_read_kernel(void *dst, u32 size, const void *unsafe_ptr)

Description:
      Safely attempt to read size bytes from kernel space address unsafe_ptr and store the data in dst.
      Return 0 on success, or a negative error in case of failure.

```

#### 2. PT_REGS_PARM*
> PT_REGS_PARM* 是在 bpf_helpers.h 中定义的宏，用于快速地从 pt_regs 结构体中获取内核函数的参数的值。PT_REGS_PARM* 包括 PT_REGS_PARM1 、 PT_REGS_PARM2 、 PT_REGS_PARM3 、 PT_REGS_PARM4 以及 PT_REGS_PARM5， 分别用于获取第一个到第五个参数的值。

#### 3. struct pt_regs
> pt_regs主要用于存放寄存器的状态，bpf程序接收到的参数为`struct pt_regs *ctx`的原因是（个人猜测）：当bpf程序所挂载的函数被调用之前，其调用者会自动地将**caller saved registers**保存在被调用函数的栈帧(Stack Frame)中，这些寄存器中存放着调用函数(caller)将要传递给被调用函数(callee)的参数，而`struct pt_regs`正是栈帧中用来存放**caller saved registers**的结构体。因此通过该结构体，bpf程序便能获得传送给被挂载函数的参数。

#### 4. sk_buff
> 套接字缓冲区结构(Socket Buffer structure)，也称为SKB或sk_buff，其在内核中创建并且用于发送/接收数据报

#### 5. caller saved registers 与 callee saved registers
> 假设有一个场景：函数A调用函数B。
> - 在函数B被正式调用之前，**caller saved register**会自动地被保存在函数B的栈帧中(Stack Frame)中，对应的指令由编译器生成，一般不存在于源代码文件中。这么做的原因是：函数A默认**caller saved registers**会被函数B修改，因此在正式调用函数B之前需要在函数B的栈帧保存这些寄存器的值，从而该函数返回时可以从其栈帧中恢复这些寄存器的值。
> - 在执行函数B时，由函数B自己决定是否保存**callee saved registers**
