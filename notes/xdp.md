#### 1. bpf\_fib\_lookup

*   `static long (*bpf_fib_lookup)(void *ctx, struct bpf_fib_lookup *params, int plen, __u32 flags)`

*   Do FIB lookup in kernel tables using parameters in *params*. If lookup is successful and result shows packet is to be forwarded, the neighbor tables are searched for the nexthop. If successful (ie., FIB lookup shows forwarding and nexthop is resolved), the nexthop address is returned in ipv4\_dst or ipv6\_dst based on family, smac is set to mac address of egress device, dmac is set to nexthop mac address, rt\_metric is set to metric from route (IPv4/IPv6 only), and ifindex is set to the device index of the nexthop from the FIB lookup.

*   *plen* argument is the size of the passed in struct. *flags* argument can be a combination of one or more of the following values:
    *(1) BPF\_FIB\_LOOKUP\_DIRECT:* Do a direct table lookup vs full lookup using FIB rules.
    (2) *BPF\_FIB\_LOOKUP\_OUTPUT:* Perform lookup from an egress perspective (default is ingress).

*   *ctx* is either *struct xdp\_md* for XDP programs or *struct sk\_buff* tc cls\_act programs.

*   Returns:
    *(1)* < 0 if any input argument is invalid.
    *(2)* 0 on success (packet is forwarded, nexthop neighbor exists).
    *(3)* > 0 one of *BPF\_FIB\_LKUP\_RET\_* codes explaining why the packet is not forwarded or needs assist from full stack.
    If lookup fails with *BPF\_FIB\_LKUP\_RET\_FRAG\_NEEDED*, then the MTU was exceeded and output params->mtu\_result contains the MTU.

*   bpf\_fib\_lookup的返回值如下:

| 返回值                                | 含义                                                                               |
| ---------------------------------- | -------------------------------------------------------------------------------- |
| BPF\_FIB\_LKUP\_RET\_SUCCESS       | success                                                                          |
| BPF\_FIB\_LKUP\_RET\_BLACKHOLE     | dest is blackholed; can be dropped                                               |
| BPF\_FIB\_LKUP\_RET\_UNREACHABLE   | dest is unreachable; can be dropped                                              |
| BPF\_FIB\_LKUP\_RET\_PROHIBIT      | dest not allowed; can be dropped                                                 |
| BPF\_FIB\_LKUP\_RET\_NOT\_FWDED    | packet is not forwarded 非转发报文                                                    |
| BPF\_FIB\_LKUP\_RET\_FWD\_DISABLED | fwding is not enabled on ingress. Do sysctl net.ipv{4,6}.conf.all.forwarding = 1 |
| BPF\_FIB\_LKUP\_RET\_UNSUPP\_LWT   | fwd requires encapsulation                                                       |
| BPF\_FIB\_LKUP\_RET\_NO\_NEIGH     | no neighbor entry for nh                                                         |
| BPF\_FIB\_LKUP\_RET\_FRAG\_NEEDED  | fragmentation required to fwd                                                    |

#### 2. bpf\_redirect\_map

*   `static long (*bpf_redirect)(__u32 ifindex, __u64 flags) = (void *) 23`

*   Redirect the packet to another net device of index *ifindex*. This helper is somewhat similar to *bpf\_clone\_redirect()*, except that the packet is not cloned, which provides increased performance.

*   Except for XDP, both ingress and egress interfaces can be used for redirection. The *BPF\_F\_INGRESS* value in *flags* is used to make the distinction (ingress path is selected if the flag is present, egress path otherwise). Currently, XDP only supports redirection to the egress interface, and accepts no flag at all.

*   The same effect can also be attained with the more generic *bpf\_redirect\_map()*, which uses a BPF map to store the redirect target instead of providing it directly to the helper.

*   Returns:
    For XDP, the helper returns *XDP\_REDIRECT* on success or
    *XDP\_ABORTED* on error. For other program types, the values
    are *TC\_ACT\_REDIRECT* ;on success or *TC\_ACT\_SHOT* on error.

