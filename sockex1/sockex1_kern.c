#include <uapi/linux/bpf.h>
#include <uapi/linux/if_ether.h>
#include <uapi/linux/if_packet.h>
#include <uapi/linux/ip.h>
#include <bpf/bpf_helpers.h>
#include "bpf_legacy.h"

// 定义BPF_MAP_TYPE_ARRAY类型的map
struct {
	__uint(type, BPF_MAP_TYPE_ARRAY);
	__type(key, u32);
	__type(value, long);
	__uint(max_entries, 256);
} my_map SEC(".maps");

SEC("socket1")
int bpf_prog1(struct __sk_buff *skb)
{	
	// unsigned long long load_byte(void *skb, unsigned long long off) asm("llvm.bpf.load.byte");
	// offsetof(type, member-designator)用于获得结构体成员相对于结构体开头的字节偏移量
	// ETH_HLEN表示以太网帧头部的长度,也可看作iphdr结构体在skb结构体中的偏移量,该结构体保存的是ip数据报头部的内容
	// 因此load_byte的第二个参数是指ip数据报中的协议id字段相对于个skb结构体开头的字节偏移量
	// 这里load_byte函数的作用是从第二个参数所指的偏移量开始,读取一个字节的数据,并赋值给index
	int index = load_byte(skb, ETH_HLEN + offsetof(struct iphdr, protocol));
	long *value;

	// pkt_type = PACKET_OUTGOING 表示当前数据包是本地回环包
	if (skb->pkt_type != PACKET_OUTGOING)
		return 0;

	// 将IP数据报头部中的协议ID作为KEY，从名为my_map的map中查找对应的值
	value = bpf_map_lookup_elem(&my_map, &index);
	// value指针变量不为空则将其所指向的数值加一
	if (value)
		__sync_fetch_and_add(value, skb->len);

	return 0;
}
char _license[] SEC("license") = "GPL";
