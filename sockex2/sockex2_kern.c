#include <uapi/linux/bpf.h>
#include <uapi/linux/in.h>
#include <uapi/linux/if.h>
#include <uapi/linux/if_ether.h>
#include <uapi/linux/ip.h>
#include <uapi/linux/ipv6.h>
#include <uapi/linux/if_tunnel.h>
#include <bpf/bpf_helpers.h>
#include "bpf_legacy.h"
#define IP_MF		0x2000
#define IP_OFFSET	0x1FFF

struct vlan_hdr {
	__be16 h_vlan_TCI;
	__be16 h_vlan_encapsulated_proto;
};

struct flow_key_record {
	__be32 src;
	__be32 dst;
	union {
		__be32 ports;
		__be16 port16[2];
	};
	__u16 thoff;
	__u8 ip_proto;
};

static inline int proto_ports_offset(__u64 proto)
{
	switch (proto) {
	case IPPROTO_TCP:
	case IPPROTO_UDP:
	case IPPROTO_DCCP:
	case IPPROTO_ESP:
	case IPPROTO_SCTP:
	case IPPROTO_UDPLITE:
		return 0;
	case IPPROTO_AH:
		return 4;
	default:
		return 0;
	}
}

// 判断当前IP4数据包是否为最后一个分片
// MF = 1时, 后面还有分片; MF = 0时, 本分片就是该分组的最后一个分片, 即后面没有分片
// 这里没有检查DF的值, 只有当DF = 0时, 分片才会有意义 
static inline int ip_is_fragment(struct __sk_buff *ctx, __u64 nhoff)
{
	return load_half(ctx, nhoff + offsetof(struct iphdr, frag_off))
		& (IP_MF | IP_OFFSET);
}

static inline __u32 ipv6_addr_hash(struct __sk_buff *ctx, __u64 off)
{
	__u64 w0 = load_word(ctx, off);
	__u64 w1 = load_word(ctx, off + 4);
	__u64 w2 = load_word(ctx, off + 8);
	__u64 w3 = load_word(ctx, off + 12);

	return (__u32)(w0 ^ w1 ^ w2 ^ w3);
}

// 解析IP4数据包
static inline __u64 parse_ip(struct __sk_buff *skb, __u64 nhoff, __u64 *ip_proto,
			     struct flow_key_record *flow)
{
	__u64 verlen;

	// 在代码块中, 在if条件判断语句中使用unlikely()函数表示: 写这个程序的人认为括号里面的值在大部分情况下都为假(0)。
	// gcc编译器识别到unlikely()函数后, 会将条件判断为假执行的代码块(也就是else中的代码)放到紧跟前面的程序位置。
	if (unlikely(ip_is_fragment(skb, nhoff)))
		*ip_proto = 0;
	else
		// 将ip4数据包头部中的协议类型信息赋值给ip_proto
		*ip_proto = load_byte(skb, nhoff + offsetof(struct iphdr, protocol));

	// 从ip4数据包头部中的协议类型中判断是否采用了通用路由封装协议(Generic Routing Encapsulation)
	// 通用路由封装协议定义了在任意一种网络层协议上封装其他任意一种网络层协议的机制, IPv4和IPv6都适用。
	if (*ip_proto != IPPROTO_GRE) {
		// ip4数据包头部中的源地址信息赋值给flow->src
		flow->src = load_word(skb, nhoff + offsetof(struct iphdr, saddr));
		// ip4数据包头部中的目的地址信息赋值给flow->dst
		flow->dst = load_word(skb, nhoff + offsetof(struct iphdr, daddr));
	}

	// 获取ipv4数据包头部的长度
	verlen = load_byte(skb, nhoff + 0/*offsetof(struct iphdr, ihl)*/);
	if (likely(verlen == 0x45))
		// 更新nhoff, ip数据包头部的最小长度为20字节; 在大部分情况下, 其头部的长度都为20字节
		nhoff += 20;
	else
		nhoff += (verlen & 0xF) << 2;

	return nhoff;
}

static inline __u64 parse_ipv6(struct __sk_buff *skb, __u64 nhoff, __u64 *ip_proto,
			       struct flow_key_record *flow)
{	
	// 将ip6数据包头部中的协议类型信息赋值给ip_proto
	*ip_proto = load_byte(skb,
			      nhoff + offsetof(struct ipv6hdr, nexthdr));
	// ip6数据包头部中的源地址信息赋值给flow->src
	flow->src = ipv6_addr_hash(skb,
				   nhoff + offsetof(struct ipv6hdr, saddr));
	// ip6数据包头部中的目的地址信息赋值给flow->dst
	flow->dst = ipv6_addr_hash(skb,
				   nhoff + offsetof(struct ipv6hdr, daddr));
	// 更新nhoff
	nhoff += sizeof(struct ipv6hdr);

	return nhoff;
}

static inline bool flow_dissector(struct __sk_buff *skb,
				  struct flow_key_record *flow)
{	
	// 以太网帧的头部长度
	__u64 nhoff = ETH_HLEN;
	__u64 ip_proto;
	// 从skb所指向的地址偏移12个字节后的位置开始读取16个比特位数据;
	// 这里获取的是以太网帧中的协议字段, 初步确定收到的数据包是属于vlan还是ip,
	// 但具体的类型还需要获取对应头结构体中的协议类型属性值才能获知, 
	// 如 proto == ETH_P_IP 代表以太网帧中封装的是一个ipv4数据包,
	// 但该数据包有可能采用了ip隧道技术而不是普通的ip数据包, 即ip头结构体中的
	// 协议类型属性值可能是IPPROTO_IPIP, IPPROTO_IPV6或者IPPROTO_GRE;
	__u64 proto = load_half(skb, 12);
	int poff;

	// VLAN数据包
	// struct vlan_hdr {
	// 	__be16	h_vlan_TCI;
	// 	__be16	h_vlan_encapsulated_proto;
	// };
	if (proto == ETH_P_8021AD) {
		// 读取vlan数据包头部中存放的协议类型信息
		proto = load_half(skb, nhoff + offsetof(struct vlan_hdr,
							h_vlan_encapsulated_proto));
		// nhoff更新为：以太网帧的头部长度 + vlan数据包头部长度
		nhoff += sizeof(struct vlan_hdr);
	}

	// VLAN数据包
	if (proto == ETH_P_8021Q) {
		// 读取vlan数据包头部中存放的协议类型信息
		proto = load_half(skb, nhoff + offsetof(struct vlan_hdr,
							h_vlan_encapsulated_proto));
		// nhoff更新为：以太网帧的头部长度 + vlan数据包头部长度
		nhoff += sizeof(struct vlan_hdr);
	}

	// IPv4数据包
	if (likely(proto == ETH_P_IP))
		// 设置folw->src,flow->dst以及ip_proto的值
		// 若采用了ip隧道技术来传输此IP数据包则需要在下面进行特殊处理
		nhoff = parse_ip(skb, nhoff, &ip_proto, flow);
	// IPv6数据包
	else if (proto == ETH_P_IPV6)
		// 设置folw->src,flow->dst以及ip_proto的值
		nhoff = parse_ipv6(skb, nhoff, &ip_proto, flow);
	// 其他类型的数据包则直接返回false
	else
		return false;

	// 接下来判断ipv4数据包是否采用了ip隧道技术, 注意此时nhoff的值已更新
	// 其值为以太网帧头部长度 + ipv4头部长度; 若采用了ip隧道技术, 那么ip
	// v4包的数据部分就会分成:内层数据包头部 + 内层数据包数据部分
	switch (ip_proto) {
	// ipv4数据包头部的协议类型为GRE(通用路由封装协议)
	case IPPROTO_GRE: {
		struct gre_hdr {
			__be16 flags;
			__be16 proto;
		};

		// 内层数据包以struct gre_hdr开头, 这里是获取该结构体flags成员的值
		__u64 gre_flags = load_half(skb,
					    nhoff + offsetof(struct gre_hdr, flags));
		// 获取gre_hdr结构体proto成员的值
		__u64 gre_proto = load_half(skb,
					    nhoff + offsetof(struct gre_hdr, proto));

		// gre_flags若设置了GRE_VERSION或GRE_ROUTING标志, 则直接退出
		if (gre_flags & (GRE_VERSION|GRE_ROUTING))
			break;

		// 获取内层数据包头部的协议类型
		proto = gre_proto;
		// 更新nhoff的值, 此时其值为：以太网帧头部长度 + ipv4头部长度 + gre_hdr的长度
		// 往后便是内层数据包的数据部分
		nhoff += 4;
		// 设置了GRE_CSUM标志
		if (gre_flags & GRE_CSUM)
			nhoff += 4;
		// 设置了GRE_KEY标志
		if (gre_flags & GRE_KEY)
			nhoff += 4;
		// 设置了GRE_SEQ标志
		if (gre_flags & GRE_SEQ)
			nhoff += 4;

		// 内层数据包头部的协议类型为ETH_P_8021Q, 代表内层数据包VLAN数据包
		if (proto == ETH_P_8021Q) {
			// 再获取VLAN数据包里面的协议类型
			proto = load_half(skb,
					  nhoff + offsetof(struct vlan_hdr,
							   h_vlan_encapsulated_proto));
			// 指向VLAN数据包中的数据部分
			nhoff += sizeof(struct vlan_hdr);
		}

		// 内层数据包头部的协议类型为ETH_P_IP, 代表内层数据包是ipv4数据包
		if (proto == ETH_P_IP)
			// 解析数据包, 并将源地址和目的地址记录在flow结构体中
			nhoff = parse_ip(skb, nhoff, &ip_proto, flow);
		// 内层数据包头部的协议类型为ETH_P_IP, 代表内层数据包是ipv6数据包
		else if (proto == ETH_P_IPV6)
			// 解析数据包, 并将源地址和目的地址记录在flow结构体中
			nhoff = parse_ipv6(skb, nhoff, &ip_proto, flow);
		else
			return false;
		break;
	}
	// ipv4数据包头部的协议类型为IPPROTO_IPIP, 即在IPv4报文的基础上再封装一个IPv4报文
	case IPPROTO_IPIP:
		// 解析被包装的ip数据包, 返回的nhoff是其数据部分在skb结构体中的偏移量
		nhoff = parse_ip(skb, nhoff, &ip_proto, flow);
		break;
	// ipv4数据包头部的协议类型为IPPROTO_IPV6, 在IPv4报文的基础上再封装一个IPv6报文
	case IPPROTO_IPV6:
		// 解析被包装的ip数据包, 返回的nhoff是其数据部分在skb结构体中的偏移量
		nhoff = parse_ipv6(skb, nhoff, &ip_proto, flow);
		break;
	default:
		break;
	}

	// 将ip_proto的值赋值给flow->ip_proto
	flow->ip_proto = ip_proto;
	// 不同协议类型的ip数据包会将端口放置到数据部分的不同位置上
	poff = proto_ports_offset(ip_proto);
	if (poff >= 0) {
		nhoff += poff;
		// 将IP数据包数据部分的端口信息赋值给flow->ports
		flow->ports = load_word(skb, nhoff);
	}

	// 将nhoff的值赋值给flow->thoff
	flow->thoff = (__u16) nhoff;

	return true;
}

struct pair {
	long packets;
	long bytes;
};

struct {
	__uint(type, BPF_MAP_TYPE_HASH);
	__type(key, __be32);
	__type(value, struct pair);
	__uint(max_entries, 1024);
} hash_map SEC(".maps");

SEC("socket2")
int bpf_prog2(struct __sk_buff *skb)
{
	struct flow_key_record flow = {};
	struct pair *value;
	u32 key;

	// 解析以太网帧,并将对应的数据赋值给flow结构体中对应的成员
	if (!flow_dissector(skb, &flow))
		return 0;

	// hash_map中元素的key是数据包的目的地址, 而元素的value则是
	// 具有相同目的地址的数据包的出现次数以及所有数据包的总字节数;
	key = flow.dst;
	// 寻找对应的元素
	value = bpf_map_lookup_elem(&hash_map, &key);
	if (value) {
		__sync_fetch_and_add(&value->packets, 1);
		__sync_fetch_and_add(&value->bytes, skb->len);
	} else {
		struct pair val = {1, skb->len};

		bpf_map_update_elem(&hash_map, &key, &val, BPF_ANY);
	}
	return 0;
}

char _license[] SEC("license") = "GPL";
