// SPDX-License-Identifier: GPL-2.0
#include <stdio.h>
#include <assert.h>
#include <linux/bpf.h>
#include <bpf/bpf.h>
#include <bpf/libbpf.h>
#include "sock_example.h"
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/resource.h>

struct pair {
	__u64 packets;
	__u64 bytes;
};

int main(int ac, char **argv)
{
	struct bpf_object *obj;
	int map_fd, prog_fd;
	char filename[256];
	int i, sock;
	FILE *f;

	snprintf(filename, sizeof(filename), "%s_kern.o", argv[0]);

	// 加载bpf目标文件中BPF_PROG_TYPE_SOCKET_FILTER类型的bpf程序
	// 并返回指向bpf_object结构体的指针和bpf程序描述符
	if (bpf_prog_load(filename, BPF_PROG_TYPE_SOCKET_FILTER,
			  &obj, &prog_fd))
		return 1;

	// 获取hash_map的文件描述符
	map_fd = bpf_object__find_map_fd_by_name(obj, "hash_map");

	// 获得loopback网络接口的套接字描述符
	sock = open_raw_sock("lo");

	// int setsockopt(int sockfd , int level, int optname, void *optval, socklen_t *optlen);
  	// sockfd：要设置的套接字描述符。level：定义选项的层次，值可以为特定协议的代码(如IPv4,IPv6,TCP,SCTP)
  	// 或通用套接字代码(SOL_SOCKET)。optname：选项名，该参数与level参数相关。optval：指向某个变量的指针，
  	// 该变量是要设置新值的缓冲区。该变量可以是一个结构体，也可以是普通变量。optlen：optval的长度。
  	// 该函数的作用是将bpf程序挂载到对应的套接字上，每当有数据通过该套接字，bpf程序便会被触发。
	assert(setsockopt(sock, SOL_SOCKET, SO_ATTACH_BPF, &prog_fd,
			  sizeof(prog_fd)) == 0);

	f = popen("ping -4 -c5 localhost", "r");
	(void) f;

	// 每隔一秒打印信息
	for (i = 0; i < 5; i++) {
		int key = 0, next_key;
		struct pair value;
		// 遍历hash_map中所有的元素
		while (bpf_map_get_next_key(map_fd, &key, &next_key) == 0) {
			// 寻找对应的元素, 并将其值赋值给value
			bpf_map_lookup_elem(map_fd, &next_key, &value);
			// 输出数据包的目的地址, 相同目的地址的数据包的总字节数以及出现次数
			printf("ip %s bytes %lld packets %lld\n",
			       inet_ntoa((struct in_addr){htonl(next_key)}),
			       value.bytes, value.packets);
			// 下一个元素的key
			key = next_key;
		}
		sleep(1);
	}
	return 0;
}
