/* SPDX-License-Identifier: GPL-2.0 */
#include <stdlib.h>
#include <stdio.h>
#include <linux/unistd.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <linux/if_ether.h>
#include <net/if.h>
#include <linux/if_packet.h>
#include <arpa/inet.h> // uint16_t htons(uint16_t hostshort)

static inline int open_raw_sock(const char *name)
{	
	// sockaddr_ll 是存放数据链路层头部信息的结构体
	struct sockaddr_ll sll;
	int sock;

	// 创建一个指定协议簇为 PF_PACKET的socket用于接收数据链路层的帧
	// socket函数的第二个参数为SOCK_RAW则代表获取的数据包是一个完整的数据链路层数据包
	// 若第二个参数为SOCK_DGRAM时,获取的数据包已去掉了数据链路层的头(link-layer header)
	// ETH_P_ALL则代表获取所有上层协议的数据链路帧,若第三个参数为ETH_P_IP则代表只接收
	// 上层协议为ip的数据链路帧
	sock = socket(PF_PACKET, SOCK_RAW | SOCK_NONBLOCK | SOCK_CLOEXEC, htons(ETH_P_ALL));
	if (sock < 0) {
		printf("cannot create raw socket\n");
		return -1;
	}

	// 用0填充sll所指向的sockaddr_ll结构体
	memset(&sll, 0, sizeof(sll));
	// 设置地址簇类型
	sll.sll_family = AF_PACKET;
	// unsigned int if_nametoindex(const char *ifname);
	// 根据网络接口名称获取网络接口索引
	sll.sll_ifindex = if_nametoindex(name);
	// uint16_t htons(uint16_t hostshort),用于将一个无符号短整型数值转换为TCP/IP网络字节序
	// 设置上层的协议类型,ETH_P_ALL代表任何的上层协议均可
	sll.sll_protocol = htons(ETH_P_ALL);

	// int bind(int sockfd, struct sockaddr *my_addr, int addrlen);
	// 使用bind函数将套接字与机器上的端口建立连接
	// sockfd 是调用 socket 返回的文件描述符。my_addr 是指向数据结构 struct sockaddr 的指针
	// 它保存你的地址(即端口和 IP 地址) 信息。 addrlen 设置为 sizeof(struct sockaddr)。
	if (bind(sock, (struct sockaddr *)&sll, sizeof(sll)) < 0) {
		printf("bind to %s: %s\n", name, strerror(errno));
		close(sock);
		return -1;
	}

	// 返回socket描述符
	return sock;
}
