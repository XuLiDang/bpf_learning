#include <uapi/linux/bpf.h>
#include <uapi/linux/in.h>
#include <uapi/linux/if.h>
#include <uapi/linux/if_ether.h>
#include <uapi/linux/ip.h>
#include <uapi/linux/ipv6.h>
#include <uapi/linux/if_tunnel.h>
#include <uapi/linux/mpls.h>
#include <bpf/bpf_helpers.h>
#include "bpf_legacy.h"
#define IP_MF		0x2000
#define IP_OFFSET	0x1FFF

#define PROG(F) SEC("socket/"__stringify(F)) int bpf_func_##F

// 定义BPF_MAP_TYPE_PROG_ARRAY类型的map, map中的元素的值都是bpf程序描述符
struct {
	__uint(type, BPF_MAP_TYPE_PROG_ARRAY);
	__uint(key_size, sizeof(u32));
	__uint(value_size, sizeof(u32));
	__uint(max_entries, 8);
} jmp_table SEC(".maps");

#define PARSE_VLAN 1
#define PARSE_MPLS 2
#define PARSE_IP 3
#define PARSE_IPV6 4

/* Protocol dispatch routine. It tail-calls next BPF program depending
 * on eth proto. Note, we could have used ...
 *
 *   bpf_tail_call(skb, &jmp_table, proto);
 *
 * ... but it would need large prog_array and cannot be optimised given
 * the map key is not static.
 */
static inline void parse_eth_proto(struct __sk_buff *skb, u32 proto)
{
	switch (proto) {
		// 以太网帧头部中的协议类型字段为ETH_P_8021Q或ETH_P_8021AD
		case ETH_P_8021Q:
		case ETH_P_8021AD:
			bpf_tail_call(skb, &jmp_table, PARSE_VLAN);
			break;
		// 以太网帧头部中的协议类型字段为ETH_P_MPLS_UC或ETH_P_MPLS_MC
		case ETH_P_MPLS_UC:
		case ETH_P_MPLS_MC:
			bpf_tail_call(skb, &jmp_table, PARSE_MPLS);
			break;
		// 以太网帧头部中的协议类型字段为ETH_P_IP
		case ETH_P_IP:
			bpf_tail_call(skb, &jmp_table, PARSE_IP);
			break;
		// 以太网帧头部中的协议类型字段为ETH_P_IPV6	
		case ETH_P_IPV6:
			bpf_tail_call(skb, &jmp_table, PARSE_IPV6);
			break;
	}
}

// VLAN数据包头结构体
struct vlan_hdr {
	__be16 h_vlan_TCI;
	__be16 h_vlan_encapsulated_proto;
};

// 记录接收到的数据包信息, 该结构体将作为hash_map的key
struct flow_key_record {
	__be32 src;
	__be32 dst;
	union {
		__be32 ports;
		__be16 port16[2];
	};
	__u32 ip_proto;
};

// 判断当前IP4数据包是否为最后一个分片
// MF = 1时, 后面还有分片; MF = 0时, 本分片就是该分组的最后一个分片, 即后面没有分片
// 这里没有检查DF的值, 只有当DF = 0时, 分片才会有意义
static inline int ip_is_fragment(struct __sk_buff *ctx, __u64 nhoff)
{
	return load_half(ctx, nhoff + offsetof(struct iphdr, frag_off))
		& (IP_MF | IP_OFFSET);
}

static inline __u32 ipv6_addr_hash(struct __sk_buff *ctx, __u64 off)
{
	__u64 w0 = load_word(ctx, off);
	__u64 w1 = load_word(ctx, off + 4);
	__u64 w2 = load_word(ctx, off + 8);
	__u64 w3 = load_word(ctx, off + 12);

	return (__u32)(w0 ^ w1 ^ w2 ^ w3);
}

struct globals {
	struct flow_key_record flow;
};

// 定义BPF_MAP_TYPE_ARRAY类型的map
struct {
	__uint(type, BPF_MAP_TYPE_ARRAY);
	__type(key, __u32);
	__type(value, struct globals);
	__uint(max_entries, 32);
} percpu_map SEC(".maps");

/* user poor man's per_cpu until native support is ready */
static struct globals *this_cpu_globals(void)
{	
	// 获得当前SMP处理器的ID
	u32 key = bpf_get_smp_processor_id();
	// 将当前SMP处理器的ID作为percpu_map的key, 返回对应元素的值
	// 元素的值为globals结构体
	return bpf_map_lookup_elem(&percpu_map, &key);
}

/* some simple stats for user space consumption */
// 该结构体将作为hash_map的value
struct pair {
	__u64 packets;
	__u64 bytes;
};

// 定义BPF_MAP_TYPE_HASH类型的map
struct {
	__uint(type, BPF_MAP_TYPE_HASH);
	__type(key, struct flow_key_record);
	__type(value, struct pair);
	__uint(max_entries, 1024);
} hash_map SEC(".maps");

static void update_stats(struct __sk_buff *skb, struct globals *g)
{	
	// SMP处理器ID作为percpu_map中的key, 而对应的value则是globals结构体
	// globals结构体中只有一个flow_key_record结构体成员, 而该成员又是hash_map
	// 中的key; 即SMP处理器ID决定对应的globals结构体, 继而决定了对应的flow_key_record结构体
	struct flow_key_record key = g->flow;
	struct pair *value;

	// 寻找对应的元素, 并将其值赋值给value; 在hash_map中, key为flow_key_record结构体
	// 而对应的value则是pair结构体
	value = bpf_map_lookup_elem(&hash_map, &key);
	if (value) {
		// 记录SMP处理器所处理的数据包
		__sync_fetch_and_add(&value->packets, 1);
		// 记录SMP处理器所处理的数据包的总字节数
		__sync_fetch_and_add(&value->bytes, skb->len);
	} else {
		struct pair val = {1, skb->len};

		bpf_map_update_elem(&hash_map, &key, &val, BPF_ANY);
	}
}

static __always_inline void parse_ip_proto(struct __sk_buff *skb,
					   struct globals *g, __u32 ip_proto)
{	
	// skb->cb[0]存放的是当前nhoff的值
	__u32 nhoff = skb->cb[0];
	int poff;


	switch (ip_proto)
	{
	// ipv4数据包头部的协议类型为GRE(通用路由封装协议)
	case IPPROTO_GRE: {
		struct gre_hdr {
			__be16 flags;
			__be16 proto;
		};

		// 内层数据包以struct gre_hdr开头, 这里是获取该结构体flags成员的值
		// 内层数据包 = ipv4数据包中的数据部分
		__u32 gre_flags = load_half(skb,
					    nhoff + offsetof(struct gre_hdr, flags));
		// 获取gre_hdr结构体proto成员的值
		__u32 gre_proto = load_half(skb,
					    nhoff + offsetof(struct gre_hdr, proto));
		// gre_flags若设置了GRE_VERSION或GRE_ROUTING标志, 则直接退出
		if (gre_flags & (GRE_VERSION|GRE_ROUTING))
			break;

		// 更新nhoff的值, 此时其指向的是内层数据包中的数据部分
		nhoff += 4;
		// 设置了GRE_CSUM标志
		if (gre_flags & GRE_CSUM)
			nhoff += 4;
		// 设置了GRE_KEY标志
		if (gre_flags & GRE_KEY)
			nhoff += 4;
		// 设置了GRE_SEQ标志
		if (gre_flags & GRE_SEQ)
			nhoff += 4;
		// 将当前nhoff的值存放在skb->cb[0]中
		skb->cb[0] = nhoff;
		parse_eth_proto(skb, gre_proto);
		break;
	}
	// IPV4数据包的协议类型为IPPROTO_IPIP, 即代表IPV4数据包中的数据部分是一个IPv4数据包
	case IPPROTO_IPIP:
		parse_eth_proto(skb, ETH_P_IP);
		break;
	// IPV4数据包头部的协议类型为IPPROTO_IPV6, 即代表IPV4数据包中的数据部分是一个IPv6数据包
	case IPPROTO_IPV6:
		parse_eth_proto(skb, ETH_P_IPV6);
		break;
	// IPV4数据包头部的协议类型为IPPROTO_TCP或IPPROTO_UDP
	// 即代表IPV4数据包中的数据部分是一个TCP或UDP数据包
	case IPPROTO_TCP:
	case IPPROTO_UDP:
		// 获取TCP或UDP数据包中的端口字段信息
		g->flow.ports = load_word(skb, nhoff);
	// IPV4数据包头部的协议类型为IPPROTO_ICMP, 即代表IPV4数据包中的数据部分是一个ICMP数据包
	case IPPROTO_ICMP:
		g->flow.ip_proto = ip_proto;
		update_stats(skb, g);
		break;
	default:
		break;
	}
}

// SEC("socket/"PARSE_IP) int bpf_func_##PARSE_IP
PROG(PARSE_IP)(struct __sk_buff *skb)
{	
	// 将当前SMP处理器ID作为key, 从percpu_map中返回对应元素的值
	struct globals *g = this_cpu_globals();
	__u32 nhoff, verlen, ip_proto;

	if (!g)
		return 0;

	// skb->cb[0]存放的是当前nhoff的值
	nhoff = skb->cb[0];

	// 判断当前IPV4数据包是否为分片
	if (unlikely(ip_is_fragment(skb, nhoff)))
		return 0;

	// 获取IPV4数据包头部的协议类型字段
	ip_proto = load_byte(skb, nhoff + offsetof(struct iphdr, protocol));

	// 从ip4数据包头部中的协议类型中判断是否采用了通用路由封装协议(Generic Routing Encapsulation)
	// 通用路由封装协议定义了在任意一种网络层协议上封装其他任意一种网络层协议的机制, IPv4和IPv6都适用。
	if (ip_proto != IPPROTO_GRE) {
		g->flow.src = load_word(skb, nhoff + offsetof(struct iphdr, saddr));
		g->flow.dst = load_word(skb, nhoff + offsetof(struct iphdr, daddr));
	}

	// 获取ipv4数据包头部的长度
	verlen = load_byte(skb, nhoff + 0/*offsetof(struct iphdr, ihl)*/);
	// 更新当前nhoff的值
	nhoff += (verlen & 0xF) << 2;
	// 将当前nhoff的值存放在skb->cb[0]中
	skb->cb[0] = nhoff;
	// 继续解析该IPV4数据包的数据部分
	parse_ip_proto(skb, g, ip_proto);
	return 0;
}

// SEC("socket/"PARSE_IPV6) int bpf_func_##PARSE_IPV6
PROG(PARSE_IPV6)(struct __sk_buff *skb)
{	
	// 将当前SMP处理器ID作为key, 从percpu_map中返回对应元素的值
	struct globals *g = this_cpu_globals();
	__u32 nhoff, ip_proto;

	if (!g)
		return 0;

	// skb->cb[0]存放的是当前nhoff的值
	nhoff = skb->cb[0];

	// 获取IPV6数据包头部的协议类型字段
	ip_proto = load_byte(skb,
			     nhoff + offsetof(struct ipv6hdr, nexthdr));
	// 获取IPV6数据包头部的源地址字段
	g->flow.src = ipv6_addr_hash(skb,
				     nhoff + offsetof(struct ipv6hdr, saddr));
	// 获取IPV6数据包头部的目的地址字段
	g->flow.dst = ipv6_addr_hash(skb,
				     nhoff + offsetof(struct ipv6hdr, daddr));
	// 更新当前nhoff的值, 使其加上IPV6头部长度值
	nhoff += sizeof(struct ipv6hdr);
	// 将当前nhoff的值存放在skb->cb[0]中
	skb->cb[0] = nhoff;
	// 继续解析该IPV6数据包的数据部分
	parse_ip_proto(skb, g, ip_proto);
	return 0;
}

// SEC("socket/"PARSE_VLAN) int bpf_func_##PARSE_VLAN
PROG(PARSE_VLAN)(struct __sk_buff *skb)
{
	__u32 nhoff, proto;

	// skb->cb[0]存放的是当前nhoff的值
	nhoff = skb->cb[0];

	// 获取VLAN数据包头部的协议类型字段
	proto = load_half(skb, nhoff + offsetof(struct vlan_hdr,
						h_vlan_encapsulated_proto));
	// 更新当前nhoff的值, 使其加上VLAN头部长度值
	nhoff += sizeof(struct vlan_hdr);
	// 将当前nhoff的值存放在skb->cb[0]中
	skb->cb[0] = nhoff;

	// 继续解析该VLAN数据包的数据部分
	parse_eth_proto(skb, proto);

	return 0;
}

// SEC("socket/"PARSE_MPLS) int bpf_func_##PARSE_MPLS
PROG(PARSE_MPLS)(struct __sk_buff *skb)
{
	__u32 nhoff, label;

	// skb->cb[0]存放的是当前nhoff的值
	nhoff = skb->cb[0];

	label = load_word(skb, nhoff);
	nhoff += sizeof(struct mpls_label);
	skb->cb[0] = nhoff;

	if (label & MPLS_LS_S_MASK) {
		__u8 verlen = load_byte(skb, nhoff);
		if ((verlen & 0xF0) == 4)
			parse_eth_proto(skb, ETH_P_IP);
		else
			parse_eth_proto(skb, ETH_P_IPV6);
	} else {
		parse_eth_proto(skb, ETH_P_MPLS_UC);
	}

	return 0;
}

SEC("socket/0")
int main_prog(struct __sk_buff *skb)
{	
	// 以太网帧的头部长度
	__u32 nhoff = ETH_HLEN;
	// 从skb所指向的地址并偏移12个字节后的位置开始读取16个比特位数据;
	// 这里获取的是以太网帧头部中的协议类型字段
	__u32 proto = load_half(skb, 12);

	// skb中的char cb[48]字段是skb信息控制块, 也就是存储每层的一些协议信息,
	// 当数据包在哪一层时, 存储的就是哪一层协议信息。
	skb->cb[0] = nhoff;
	parse_eth_proto(skb, proto);
	return 0;
}

char _license[] SEC("license") = "GPL";
