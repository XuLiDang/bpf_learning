// SPDX-License-Identifier: GPL-2.0
#include <stdio.h>
#include <assert.h>
#include <bpf/bpf.h>
#include <bpf/libbpf.h>
#include "sock_example.h"
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/resource.h>

struct flow_key_record {
	__be32 src;
	__be32 dst;
	union {
		__be32 ports;
		__be16 port16[2];
	};
	__u32 ip_proto;
};

struct pair {
	__u64 packets;
	__u64 bytes;
};

int main(int argc, char **argv)
{
	int i, sock, key, fd, main_prog_fd, jmp_table_fd, hash_map_fd;
	struct bpf_program *prog;
	struct bpf_object *obj;
	const char *section;
	char filename[256];
	FILE *f;

	snprintf(filename, sizeof(filename), "%s_kern.o", argv[0]);

	obj = bpf_object__open_file(filename, NULL);
	if (libbpf_get_error(obj)) {
		fprintf(stderr, "ERROR: opening BPF object file failed\n");
		return 0;
	}

	/* load BPF program */
	if (bpf_object__load(obj)) {
		fprintf(stderr, "ERROR: loading BPF object file failed\n");
		goto cleanup;
	}

	// 获得jmp_table以及hash_map的描述符
	jmp_table_fd = bpf_object__find_map_fd_by_name(obj, "jmp_table");
	hash_map_fd = bpf_object__find_map_fd_by_name(obj, "hash_map");
	if (jmp_table_fd < 0 || hash_map_fd < 0) {
		fprintf(stderr, "ERROR: finding a map in obj file failed\n");
		goto cleanup;
	}

	// 遍历bpf目标文件中的bpf程序
	bpf_object__for_each_program(prog, obj) {
		// 获得当前bpf程序的描述符
		fd = bpf_program__fd(prog);
		// 获得段名, 即用SEC所定义的名称
		section = bpf_program__section_name(prog);
		// 将段名中的socket后面的数字赋值给key, 如在"socket/0"中, 则是将0赋值给key
		if (sscanf(section, "socket/%d", &key) != 1) {
			fprintf(stderr, "ERROR: finding prog failed\n");
			goto cleanup;
		}
		// 将段名为"socket/0"的bpf程序的描述符赋值给main_prog_fd
		if (key == 0)
			main_prog_fd = fd;
		// 对于bpf目标文件中的其他bpf程序, 则将它们的描述符作为
		// jmp_table_fd的值, 而key则是段名中socket后面的数字
		else
			bpf_map_update_elem(jmp_table_fd, &key, &fd, BPF_ANY);
	}

	// 获得loopback网络接口的套接字描述符
	sock = open_raw_sock("lo");

	/* attach BPF program to socket */
	// int setsockopt(int sockfd , int level, int optname, void *optval, socklen_t *optlen);
  	// sockfd：要设置的套接字描述符。level：定义选项的层次，值可以为特定协议的代码(如IPv4,IPv6,TCP,SCTP)
  	// 或通用套接字代码(SOL_SOCKET)。optname：选项名，该参数与level参数相关。optval：指向某个变量的指针，
  	// 该变量是要设置新值的缓冲区。该变量可以是一个结构体，也可以是普通变量。optlen：optval的长度。
  	// 该函数的作用是将bpf程序挂载到对应的套接字上，每当有数据通过该套接字，bpf程序便会被触发。
	assert(setsockopt(sock, SOL_SOCKET, SO_ATTACH_BPF, &main_prog_fd,
			  sizeof(__u32)) == 0);

	// 向shell输入命令
	if (argc > 1)
		f = popen("ping -4 -c5 localhost", "r");
	else
		f = popen("netperf -l 4 localhost", "r");
	(void) f;

	// 每隔一秒打印信息
	for (i = 0; i < 5; i++) {
		struct flow_key_record key = {}, next_key;
		struct pair value;

		sleep(1);
		printf("IP     src.port -> dst.port               bytes      packets\n");
		// 遍历hash_map中所有的元素
		while (bpf_map_get_next_key(hash_map_fd, &key, &next_key) == 0) {
			// 寻找对应的元素, 并将其值赋值给value
			bpf_map_lookup_elem(hash_map_fd, &next_key, &value);
			// 输出数据包的 源地址.端口 和 目的地址.端口,
			// 同一SMP处理器所处理的数据包数以及所处理的总字节数
			printf("%s.%05d -> %s.%05d %12lld %12lld\n",
			       inet_ntoa((struct in_addr){htonl(next_key.src)}),
			       next_key.port16[0],
			       inet_ntoa((struct in_addr){htonl(next_key.dst)}),
			       next_key.port16[1],
			       value.bytes, value.packets);
			key = next_key;
		}
	}

cleanup:
	bpf_object__close(obj);
	return 0;
}
