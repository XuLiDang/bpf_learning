#### 注意事项:
- 在原始版本的`tracex1_kern.c`文件中，会将bpf程序附加到名为`__netif_receive_skb_core`的kprobe上。但在linux5.15内核中，该kprobe已失效，运行加载程序时也会报找不到对应kprobe的错误，因此我们需要修改bpf程序所附加的kprobe。

- 可在`/sys/kernel/debug/tracing/events/`目录下查看当前版本的内核所支持的所有tracepoints，同时可通过`cat /proc/kallsyms`命令查看当前内核所支持的所有kprobe

####  bpf程序的工作流程:
-  **1.** 通过kprobe挂载到内核函数`__netif_receive_skb_core`中。

- **2.** 每次执行`__netif_receive_skb_core`函数都会运行我们所定义的`bpf_prog1`函数，该函数会查找发送到loopack接口上的数据包，并通过`bpf_trace_printk`函数将数据包的信息写入到位于`/sys/kernel/debug/tracing/trace_pipe`的特殊文件中。

- **3.** 加载程序(tracex1_user.c)在加载完bpf程序后便会调用`read_trace_pipe`函数来读取`/sys/kernel/debug/tracing/trace_pipe`文件中的内容。`read_trace_pipe`函数的定义如下:
```
void read_trace_pipe(void)
{
	int trace_fd;

	trace_fd = open(DEBUGFS "trace_pipe", O_RDONLY, 0);
	if (trace_fd < 0)
		return;

	while (1) {
		static char buf[4096];
		ssize_t sz;

		sz = read(trace_fd, buf, sizeof(buf) - 1);
		if (sz > 0) {
			buf[sz] = 0;
			puts(buf);
		}
	}
}
```
#### 运行结果:
- 在samples/bpf目录下输入命令`./tracex1 &`便可运行加载程序将bpf程序挂载到对应内核函数中，并且让加载程序转入后台运行
- 随后输入`ping -c5 localhost`命令向loopback接口发送5个ICMP数据报:

![image.png](https://gitlab.com/XuLiDang/bpf_learning/-/raw/master/tracex1/result.png?inline=false)
