#include <linux/skbuff.h>
#include <linux/netdevice.h>
#include <uapi/linux/bpf.h>
#include <linux/version.h>
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_tracing.h>

// 将P的值复制给val,并返回val,val的类型与P一致
#define _(P)                                                                   \
	({                                                                     \
		typeof(P) val = 0;                                             \
		bpf_probe_read_kernel(&val, sizeof(val), &(P));                \
		val;                                                           \
	})

// __netif_receive_skb_core是网络协议栈的入口
// SEC("kprobe/__netif_receive_skb_core")
SEC("kprobe/__netif_receive_skb_core.constprop.0")
int bpf_prog1(struct pt_regs *ctx)
{
	// attaches to kprobe __netif_receive_skb_core,looks for packets on loobpack device and prints them	 
	char devname[IFNAMSIZ];
	struct net_device *dev;
	struct sk_buff *skb;
	int len;

	/* non-portable! works for the given kernel only */
	// PT_REGS_PARM1 用于从pt_regs结构体中获取bpf程序所挂载的内核函数的第一个参数值
	// 随后将该参数值复制到skb所指向的sk_buff结构体中
	bpf_probe_read_kernel(&skb, sizeof(skb), (void *)PT_REGS_PARM1(ctx));
	// 将sk_buff中的dev属性赋值给dev
	dev = _(skb->dev);
	// 将sk_buff中的len属性赋值给len
	len = _(skb->len);

	// 从dev->name所指向的内核空间地址开始,将sizeof(devname)个字节的数据复制到devname所指的地址中
	bpf_probe_read_kernel(devname, sizeof(devname), dev->name);

	// 判断设备名称是否为lo,即loopback设备
	if (devname[0] == 'l' && devname[1] == 'o')
	{
		char fmt[] = "skb %p len %d\n";
		/* using bpf_trace_printk() for DEBUG ONLY */
		// 打印符合要求的skb_buff的地址以及其长度信息
		bpf_trace_printk(fmt, sizeof(fmt), skb, len);
	}

	return 0;
}

char _license[] SEC("license") = "GPL";
u32 _version SEC("version") = LINUX_VERSION_CODE;
