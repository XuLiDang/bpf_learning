#### 注意事项:
- 在原始版本的`tracex2_kern.c`文件中，会将bpf程序附加到名为`kfree_skb`的kprobe上。但在linux5.15内核中，该kprobe已失效，运行加载程序时也会报找不到对应kprobe的错误，因此我们需要修改bpf程序所附加的kprobe。

- 可在`/sys/kernel/debug/tracing/events/`目录下查看当前版本的内核所支持的所有tracepoints，同时可通过`cat /proc/kallsyms`命令查看当前内核所支持的所有kprobe

#### 程序功能解析：
##### tracex2_kern.c：
- `tracex2_kern.c`文件中定义了两个bpf程序：**bpf_prog2**以及**bpf_prog3**。前者通过kprobe挂载到`__kfree_skb`函数中，而后者则是通过kprobe挂载到`write`系统调用中。

- 每当`__kfree_skb`函数运行时，**bpf_prog2**程序便会运行，该程序首先会从pt_regs结构体中获取ip寄存器的值，即执行完`__kfree_skb`函数后的返回地址，随后将该返回地址作为my_map中的key，最后将该key对应的元素值加一。

- 每当`sys_write`函数运行时，**bpf_prog3**程序便会运行，该程序首先会获取用户程序调用write系统调用时传入的写入字节数参数，当前运行的进程id以及用户id和当前执行的命令，并且用一个名为hist_key结构体存储这些数据。最后将该结构体作为my_hist_map中的key，并将该key对应的元素值加一。

##### tracex2_user.c：
- `tracex2_user.c`文件中的主体代码逻辑则是：首先加载bpf目标文件并将其中的bpf程序挂载到对应的内核函数中，同时执行对应的命令使系统执行bpf程序所挂载的内核函数，最后按照需求输出my_map以及my_hist_map中的内容。