#include <linux/skbuff.h>
#include <linux/netdevice.h>
#include <linux/version.h>
#include <uapi/linux/bpf.h>
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_tracing.h>
#include "trace_common.h"

// 创建HASH类型的map
struct {
	__uint(type, BPF_MAP_TYPE_HASH);
	__type(key, long);
	__type(value, long);
	__uint(max_entries, 1024);
} my_map SEC(".maps");

// SEC("kprobe/kfree_skb")
SEC("kprobe/__kfree_skb")
int bpf_prog2(struct pt_regs *ctx)
{
	long loc = 0;
	long init_val = 1;
	long *value;

	// read ip of kfree_skb caller,non-portable version of __builtin_return_address(0)
	// 从pt_regs结构体中获取ip寄存器的值，即执行完__kfree_skb函数后的返回地址
	BPF_KPROBE_READ_RET_IP(loc, ctx);

	// 从my_map中查找key为loc的值
	value = bpf_map_lookup_elem(&my_map, &loc);
	// 找到则将其值加1
	if (value)
		*value += 1;
	else
		// 值为0则将key为loc的值置为1
		bpf_map_update_elem(&my_map, &loc, &init_val, BPF_ANY);
	return 0;
}

static unsigned int log2(unsigned int v)
{
	unsigned int r;
	unsigned int shift;

	r = (v > 0xFFFF) << 4; v >>= r;
	shift = (v > 0xFF) << 3; v >>= shift; r |= shift;
	shift = (v > 0xF) << 2; v >>= shift; r |= shift;
	shift = (v > 0x3) << 1; v >>= shift; r |= shift;
	r |= (v >> 1);
	return r;
}

static unsigned int log2l(unsigned long v)
{
	unsigned int hi = v >> 32;
	if (hi)
		return log2(hi) + 32;
	else
		return log2(v);
}

// 作为my_hist_map的key
struct hist_key {
	char comm[16];
	u64 pid_tgid;
	u64 uid_gid;
	u64 index;
};

// 创建PERCPU_HASH类型的map
struct {
	__uint(type, BPF_MAP_TYPE_PERCPU_HASH);
	__uint(key_size, sizeof(struct hist_key));
	__uint(value_size, sizeof(long));
	__uint(max_entries, 1024);
} my_hist_map SEC(".maps");

SEC("kprobe/" SYSCALL(sys_write))
int bpf_prog3(struct pt_regs *ctx)
{	
	// 获取传入给内核函数sys_write的第三个参数(要写入的数据大小)
	long write_size = PT_REGS_PARM3(ctx);
	long init_val = 1;
	long *value;
	struct hist_key key;

	key.index = log2l(write_size);
	// 获得当前线程的id以及线程所属的线程组的id,从内核的角度来看:线程的id即为pid,而线程组的id则为
	// 线程所属的进程的id,具体可参考《If threads share the same PID, how can they be identified?》
	key.pid_tgid = bpf_get_current_pid_tgid();
	// 获取用户id和用户所属的组id
	key.uid_gid = bpf_get_current_uid_gid();
	// 将当前task的comm属性赋值给首地址为key.comm的字符数组,即获取当前执行的命令
	bpf_get_current_comm(&key.comm, sizeof(key.comm));

	// 从my_hist_map中查找key为key的值
	value = bpf_map_lookup_elem(&my_hist_map, &key);
	if (value)
		__sync_fetch_and_add(value, 1);
	else
		// 值为0则将key为loc的值置为1
		bpf_map_update_elem(&my_hist_map, &key, &init_val, BPF_ANY);
	return 0;
}

char _license[] SEC("license") = "GPL";
u32 _version SEC("version") = LINUX_VERSION_CODE;
