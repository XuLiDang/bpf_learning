#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <sys/resource.h>

#include <bpf/bpf.h>
#include <bpf/libbpf.h>
#include "bpf_util.h"

#define MAX_INDEX	64
#define MAX_STARS	38

/* my_map, my_hist_map */
// 用于存放tracex2_kern.c中定义的两个map的文件描述符
static int map_fd[2];

static void stars(char *str, long val, long max, int width)
{
	int i;

	for (i = 0; i < (width * val / max) - 1 && i < width - 1; i++)
		str[i] = '*';
	if (val > max)
		str[i - 1] = '+';
	str[i] = '\0';
}

struct task {
	char comm[16];
	__u64 pid_tgid;
	__u64 uid_gid;
};

struct hist_key {
	struct task t;
	__u32 index;
};

#define SIZE sizeof(struct task)

// 用于打印write系统调用函数的写入数据量分布情况
static void print_hist_for_pid(int fd, void *task)
{	
	// 获取当前机器CPU的数量
	unsigned int nr_cpus = bpf_num_possible_cpus();
	struct hist_key key = {}, next_key;
	long values[nr_cpus];
	char starstr[MAX_STARS];
	long value;
	long data[MAX_INDEX] = {};
	int max_ind = -1;
	long max_value = 0;
	int i, ind;

	while (bpf_map_get_next_key(fd, &key, &next_key) == 0)
	{	
		// 将tasks和&next_key的前SIZE个字节进行比较
		// 如果返回值=0,则表示两者相等;SIZE = sizeof(struct task)
		// 这里的含义是在my_hist_map寻找与当前task对应的元素的key
		if (memcmp(&next_key, task, SIZE)) {
			key = next_key;
			continue;
		}
		// 从my_hist_map寻找key为next_key的元素,并将其值存放在values数组中
		bpf_map_lookup_elem(fd, &next_key, values);
		value = 0;
		// my_hist_map是PERCPU_HASH类型的map,因此会为每个CPU都创建一个my_hist_map
		// 因此统计my_hist_map中某个元素的value时,需要累加该元素在每个CPU上的value
		for (i = 0; i < nr_cpus; i++)
			value += values[i];
		// next_key.index存放的是一次write系统调用函数的写入量,这里的意思是将与当前task对应的元素
		// 的key中的index属性作为data数组的索引,而该元素的value(次数)则作为data数组中索引的值
		ind = next_key.index;
		data[ind] = value;
		// 更新最大写入量以及写入次数
		if (value && ind > max_ind)
			max_ind = ind;
		if (value > max_value)
			max_value = value;
		key = next_key;
	}

	printf("           syscall write() stats\n");
	printf("     byte_size       : count     distribution\n");
	for (i = 1; i <= max_ind + 1; i++) {
		stars(starstr, data[i - 1], max_value, MAX_STARS);
		printf("%8ld -> %-8ld : %-8ld |%-*s|\n",
		       (1l << i) >> 1, (1l << i) - 1, data[i - 1],
		       MAX_STARS, starstr);
	}
}

static void print_hist(int fd)
{
	struct hist_key key = {}, next_key;
	static struct task tasks[1024];
	int task_cnt = 0;
	int i;

	// 第一个参数是map的文件描述符; 第二个是参数是"look_up key",即表明从map的哪一个key开始查找
	// 第三个参数是next_key,若map中存在lookup_key的下一个key,该函数便会返回0,并且会将下一个key赋值给next_key
	// 若当前传入函数的lookup_key不存在于map中,那么函数返回之后next_key便是map中第一个元素的key
	// 整个while循环的作用是将my_hist_map中每个元素的key的前SIZE个字节的内容复制到tasks数组中
	while (bpf_map_get_next_key(fd, &key, &next_key) == 0) {
		int found = 0;

		for (i = 0; i < task_cnt; i++)
			// 将&tasks[i]和&next_key的前SIZE个字节进行比较
			// 如果返回值=0,则表示两者相等;SIZE = sizeof(struct task)
			if (memcmp(&tasks[i], &next_key, SIZE) == 0)
				found = 1;
		if (!found)
			// 将&next_key的前SIZE个字节复制到&tasks[task_cnt]
			memcpy(&tasks[task_cnt++], &next_key, SIZE);
		key = next_key;
	}

	// 打印信息
	for (i = 0; i < task_cnt; i++) {
		// 打印task结构体中的pid,cmd,uid信息
		printf("\npid %d cmd %s uid %d\n",
		       (__u32) tasks[i].pid_tgid,
		       tasks[i].comm,
		       (__u32) tasks[i].uid_gid);
		// 打印write()系统调用的状态信息
		print_hist_for_pid(fd, &tasks[i]);
	}

}

static void int_exit(int sig)
{
	print_hist(map_fd[1]);
	exit(0);
}

int main(int ac, char **argv)
{
	long key, next_key, value;
	struct bpf_link *links[2];
	struct bpf_program *prog;
	struct bpf_object *obj;
	int i, j = 0;
	FILE *f;

	obj = bpf_object__open_file("tracex2_kern.o", NULL);
	if (libbpf_get_error(obj)) {
		fprintf(stderr, "ERROR: opening BPF object file failed\n");
		return 0;
	}

	/* load BPF program */
	if (bpf_object__load(obj)) {
		fprintf(stderr, "ERROR: loading BPF object file failed\n");
		goto cleanup;
	}

	// 获取tracex2_kern.c中定义的两个map的文件描述符
	map_fd[0] = bpf_object__find_map_fd_by_name(obj, "my_map");
	map_fd[1] = bpf_object__find_map_fd_by_name(obj, "my_hist_map");
	if (map_fd[0] < 0 || map_fd[1] < 0) {
		fprintf(stderr, "ERROR: finding a map in obj file failed\n");
		goto cleanup;
	}

	// 指定int_exit函数来处理(Signal Interrupt)SIGINT信号;如ctrl-C,通常由用户生成
	signal(SIGINT, int_exit);
	// 指定int_exit函数来处理(Signal Terminate)SIGTERM信号,即发送给本程序的终止请求信号
	signal(SIGTERM, int_exit);

	/* start 'ping' in the background to have some kfree_skb events */
	f = popen("ping -4 -c5 localhost", "r");
	(void) f;

	/* start 'dd' in the background to have plenty of 'write' syscalls */
	f = popen("dd if=/dev/zero of=/dev/null count=5000000", "r");
	(void) f;

	// 从目标文件"tracex2_kern.o"中遍历bpf程序
	bpf_object__for_each_program(prog, obj)
	{	
		// 挂载bpf程序并返回bpf_link的指针变量
		links[j] = bpf_program__attach(prog);
		if (libbpf_get_error(links[j]))
		{
			fprintf(stderr, "ERROR: bpf_program__attach failed\n");
			links[j] = NULL;
			goto cleanup;
		}
		j++;
	}

	// 每个一秒遍历并打印my_map的信息
	for (i = 0; i < 5; i++)
	{
		key = 0;
		// 第一个参数是map的文件描述符; 第二个是参数是"look_up key",即表明从map的哪一个key开始查找
		// 第三个参数是next_key,若map中存在lookup_key的下一个key,该函数便会返回0,并且会将下一个key赋值给next_key
		// 若当前传入函数的lookup_key不存在于map中,那么函数返回之后next_key便是map中第一个元素的key
		while (bpf_map_get_next_key(map_fd[0], &key, &next_key) == 0)
		{	
			// 从map中查找key为next_key的元素,并将对应的值存在到value中
			bpf_map_lookup_elem(map_fd[0], &next_key, &value);
			// 打印信息
			printf("location 0x%lx count %ld\n", next_key, value);
			key = next_key;
		}
		if (key)
			printf("\n");
		sleep(1);
	}

	print_hist(map_fd[1]);

cleanup:
	for (j--; j >= 0; j--)
		bpf_link__destroy(links[j]);

	bpf_object__close(obj);
	return 0;
}
