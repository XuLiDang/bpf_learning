#### 程序功能解析：
##### tracex3_kern.c：
- `tracex2_kern.c`文件中定义了两个bpf程序：**bpf_prog1**以及**bpf_prog2**。前者通过kprobe挂载到`blk_mq_start_request`函数中，而后者则是通过kprobe挂载到`blk_account_io_done`系统调用中。

- 每当`blk_mq_start_request`函数运行时，**bpf_prog1**程序便会运行，该程序首先会从pt_regs结构体中获取传入该函数的第一个参数(并将该参数赋值给变量rq)，即指向request结构体的指针，随后获取到目前为止机器运行的时间(以ns为单位，并赋值给变量val)。随后将变量rq作为my_map的key，而将变量val作为对应的value来更新my_map。

- 每当`blk_account_io_done`函数运行时，**bpf_prog2**程序便会运行，该程序首先会从pt_regs结构体中获取传入该函数的第一个参数(并将该参数赋值给变量rq)，即指向request结构体的指针。随后将rq变量作为key从my_map中查找对应的元素的值，并当前时间减去对应元素的值，最终得到一个差值。 结合前面我们可以得知，该差值便是从块层开始处理对应IO请求到该IO请求处理完毕所花费的时间。最后该程序将得到的差值作为lat_map的key，并将该key所对应的元素的value加一

##### tracex3_user.c：
- `tracex3_user.c`文件中的主体代码逻辑则是：首先加载bpf目标文件并将其中的bpf程序挂载到对应的内核函数中，随后按照需求输出my_map以及lat_map中的内容，简单来说就是输出I/O请求的延迟(即完成I/O请求所耗费的时间)以及延迟出现的频率。