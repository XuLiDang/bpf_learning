#include <linux/skbuff.h>
#include <linux/netdevice.h>
#include <linux/version.h>
#include <uapi/linux/bpf.h>
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_tracing.h>

// 创建BPF_MAP_TYPE_HASH类型的map
struct {
	__uint(type, BPF_MAP_TYPE_HASH);
	__type(key, long);
	__type(value, u64);
	__uint(max_entries, 4096);
} my_map SEC(".maps");

// void blk_mq_start_request(struct request *rq)
/**
 * blk_mq_start_request - Start processing a request
 * @rq: Pointer to request to be started
 *
 * Function used by device drivers to notify the block layer that a request
 * is going to be processed now, so blk layer can do proper initializations
 * such as starting the timeout timer.
 */
SEC("kprobe/blk_mq_start_request")
int bpf_prog1(struct pt_regs *ctx)
{	
	// 获得所挂载内核函数的第一个参数
	long rq = PT_REGS_PARM1(ctx);
	// Return the time elapsed since system boot, in nanoseconds.
	u64 val = bpf_ktime_get_ns();
	// 将获取的rq作为map的键,并更新它的值
	bpf_map_update_elem(&my_map, &rq, &val, BPF_ANY);
	return 0;
}

static unsigned int log2l(unsigned long long n)
{
#define S(k) if (n >= (1ull << k)) { i += k; n >>= k; }
	int i = -(n == 0);
	S(32); S(16); S(8); S(4); S(2); S(1);
	return i;
#undef S
}

#define SLOTS 100

// 创建BPF_MAP_TYPE_PERCPU_ARRAY类型的map
struct {
	__uint(type, BPF_MAP_TYPE_PERCPU_ARRAY);
	__uint(key_size, sizeof(u32));
	__uint(value_size, sizeof(u64));
	__uint(max_entries, SLOTS);
} lat_map SEC(".maps");

// static inline void blk_account_io_done(struct request *req, u64 now)
SEC("kprobe/blk_account_io_done")
int bpf_prog2(struct pt_regs *ctx)
{	
	// 获得所挂载内核函数的第一个参数
	long rq = PT_REGS_PARM1(ctx);
	u64 *value, l, base;
	u32 index;

	// 从my_map中查找key为rq的元素,并返回该元素的value
	value = bpf_map_lookup_elem(&my_map, &rq);
	// value为0则直接返回
	if (!value)
		return 0;
	// Return the time elapsed since system boot, in nanoseconds.
	u64 cur_time = bpf_ktime_get_ns();
	// 获得时间差值,根据bpf_prog1函数可知,这里是获得从块层处理IO请求开始到
	// IO请求处理完毕所花费的时间
	u64 delta = cur_time - *value;

	// 删除my_map中key为rq的元素,意味着其代表的IO请求已结束
	bpf_map_delete_elem(&my_map, &rq);

	/* the lines below are computing index = log10(delta)*10
	 * using integer arithmetic
	 * index = 29 ~ 1 usec
	 * index = 59 ~ 1 msec
	 * index = 89 ~ 1 sec
	 * index = 99 ~ 10sec or more
	 * log10(x)*10 = log2(x)*10/log2(10) = log2(x)*3
	 */
	// 根据delta来计算出对应的索引
	l = log2l(delta);
	base = 1ll << l;
	index = (l * 64 + (delta - base) * 64 / base) * 3 / 64;

	// 因为lat_map是数组类型的map,因此索引不能大于SLOTS - 1
	if (index >= SLOTS)
		index = SLOTS - 1;

	// 从lat_map中查找key为index的元素,并返回该元素的value
	// 注意lat_map为ARRAY类型的map,因此只要key的值为0-SLOTS-1以内
	// 其值均存在于map中,初始化为0
	value = bpf_map_lookup_elem(&lat_map, &index);
	// value不为0则将其加1
	if (value)
		*value += 1;

	return 0;
}
char _license[] SEC("license") = "GPL";
u32 _version SEC("version") = LINUX_VERSION_CODE;
