// SPDX-License-Identifier: GPL-2.0-only
/* Copyright (c) 2013-2015 PLUMgrid, http://plumgrid.com
 */
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <sys/resource.h>

#include <bpf/bpf.h>
#include <bpf/libbpf.h>
#include "bpf_util.h"

#define SLOTS 100

static void clear_stats(int fd)
{	
	// 获得系统可用的CPU数
	unsigned int nr_cpus = bpf_num_possible_cpus();
	__u64 values[nr_cpus];
	__u32 key;

	// 用0填充values数组
	memset(values, 0, sizeof(values));
	// 将lat_map中的所有元素的值都置为0
	for (key = 0; key < SLOTS; key++)
		bpf_map_update_elem(fd, &key, values, BPF_ANY);
}
// 用于在color模式下表示IO latency的热度
const char *color[] = {
	"\033[48;5;255m",
	"\033[48;5;252m",
	"\033[48;5;250m",
	"\033[48;5;248m",
	"\033[48;5;246m",
	"\033[48;5;244m",
	"\033[48;5;242m",
	"\033[48;5;240m",
	"\033[48;5;238m",
	"\033[48;5;236m",
	"\033[48;5;234m",
	"\033[48;5;232m",
};
// ARRAY_SIZE(color) = 12
const int num_colors = ARRAY_SIZE(color);

const char nocolor[] = "\033[00m";
// 用于在text-only模式下表示IO latency的热度
const char *sym[] = {
	" ",
	" ",
	".",
	".",
	"*",
	"*",
	"o",
	"o",
	"O",
	"O",
	"#",
	"#",
};

bool full_range = false;
bool text_only = false;

static void print_banner(void)
{
	if (full_range)
		printf("|1ns     |10ns     |100ns    |1us      |10us     |100us"
		       "    |1ms      |10ms     |100ms    |1s       |10s\n");
	else
		printf("|1us      |10us     |100us    |1ms      |10ms     "
		       "|100ms    |1s       |10s\n");
}

static void print_hist(int fd)
{	
	// 获取当前可用的CPU数量
	unsigned int nr_cpus = bpf_num_possible_cpus();
	__u64 total_events = 0;
	long values[nr_cpus];
	__u64 max_cnt = 0;
	__u64 cnt[SLOTS];
	__u64 value;
	__u32 key;
	int i;

	// SLOTS为lat_map的长度
	for (key = 0; key < SLOTS; key++) {
		// 从lat_map中查找key为key的元素,并将其值存放到values数组中
		bpf_map_lookup_elem(fd, &key, values);
		value = 0;
		// 因为lat_map是BPF_MAP_TYPE_PERCPU_ARRAY类型的map
		// 因此每个元素的value均等于其在所有CPU上的value的总和
		for (i = 0; i < nr_cpus; i++)
			value += values[i];
		// cnt数组的索引是lat_map中的key,而对应的值则是上面的value
		cnt[key] = value;
		// 更新总事件数
		total_events += value;
		// 更新max_cnt
		if (value > max_cnt)
			max_cnt = value;
	}
	// 将lat_map中的所有元素的值都置为0
	clear_stats(fd);
	// 输出IO事件的总数以及每个事件的延迟
	for (key = full_range ? 0 : 29; key < SLOTS; key++) {
		int c = num_colors * cnt[key] / (max_cnt + 1);

		if (text_only)
			printf("%s", sym[c]);
		else
			// nocolor = "\033[00m"
			printf("%s %s", color[c], nocolor);
	}
	printf(" # %lld\n", total_events);
}

int main(int ac, char **argv)
{
	struct bpf_link *links[2];
	struct bpf_program *prog;
	struct bpf_object *obj;
	char filename[256];
	int map_fd, i, j = 0;

	// 判断输入的参数
	for (i = 1; i < ac; i++) {
		// ./tracex3 -a
		if (strcmp(argv[i], "-a") == 0) {
			full_range = true;
		}
		// ./tracex3 -t 
		else if (strcmp(argv[i], "-t") == 0) {
			text_only = true;
		}
		// ./tracex3 -h
		else if (strcmp(argv[i], "-h") == 0) {
			printf("Usage:\n"
			       "  -a display wider latency range\n"
			       "  -t text only\n");
			return 1;
		}
	}

	snprintf(filename, sizeof(filename), "%s_kern.o", argv[0]);
	// 打开bpf目标文件,并返回指向bpf_object结构体的指针
	obj = bpf_object__open_file(filename, NULL);
	if (libbpf_get_error(obj)) {
		fprintf(stderr, "ERROR: opening BPF object file failed\n");
		return 0;
	}

	/* load BPF program */
	if (bpf_object__load(obj)) {
		fprintf(stderr, "ERROR: loading BPF object file failed\n");
		goto cleanup;
	}

	// 通过指向bpf_object结构体的指针以及map名称查找对应的map
	map_fd = bpf_object__find_map_fd_by_name(obj, "lat_map");
	if (map_fd < 0) {
		fprintf(stderr, "ERROR: finding a map in obj file failed\n");
		goto cleanup;
	}

	// 将bpf目标文件中的所有bpf程序都挂载到对应的内核函数中
	bpf_object__for_each_program(prog, obj){
		links[j] = bpf_program__attach(prog);
		if (libbpf_get_error(links[j])){
			fprintf(stderr, "ERROR: bpf_program__attach failed\n");
			links[j] = NULL;
			goto cleanup;
		}
		j++;
	}

	printf("  heatmap of IO latency\n");
	if (text_only)
		printf("  %s", sym[num_colors - 1]); // sym[num_colors - 1] = "#"
	else
		printf("  %s %s", color[num_colors - 1], nocolor); // olor[num_colors - 1] = "\033[48;5;232m"
	printf(" - many events with this latency\n");

	if (text_only)
		printf("  %s", sym[0]); // sym[0] = " "
	else
		printf("  %s %s", color[0], nocolor); // color[0] = "\033[48;5;255m"
	printf(" - few events\n");

	for (i = 0; ; i++) {
		if (i % 20 == 0)
			// 每20S执行一次
			print_banner();
		// 2S执行一次
		print_hist(map_fd);
		sleep(2);
	}

cleanup:
	for (j--; j >= 0; j--)
		bpf_link__destroy(links[j]);

	bpf_object__close(obj);
	return 0;
}
