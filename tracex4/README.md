#### 程序功能解析：
##### tracex4_kern.c：
- `tracex4_kern.c`文件中定义了两个bpf程序：**bpf_prog1**以及**bpf_prog2**。前者通过kprobe挂载到`kmem_cache_free`函数中，而后者则是通过kretprobe(注意这里不是kprobe)挂载到`kmem_cache_alloc_node`系统调用中。

- 每当`kmem_cache_alloc_node`函数执行到ret指令返回时，**bpf_prog2**程序便会运行，该程序首先会获取pt_regs结构体所存放的`kmem_cache_alloc_node`函数返回值和IP寄存器的值，并分别赋值给ptr以及ip变量。其中ptr变量存放的是数据对象的首地址，ip变量存放的是`kmem_cache_alloc_node`函数的返回地址。随后将ip的值赋值给pair结构体中ip属性。最后将ptr作为my_map的key，将pair结构体作为对应元素的value。

- 每当`kmem_cache_free`函数运行时，**bpf_prog1**程序便会运行，该程序首先会从pt_regs结构体中获取传送给该函数的第二个参数的值，并赋值给ptr变量，这里的ptr变量存放的同样是数据对象的首地址。随后删除my_map中key等于当前ptr变量的元素。

##### tracex4_user.c：
- `tracex4_user.c`文件中的主体代码逻辑则是：首先加载bpf目标文件并将其中的bpf程序挂载到对应的内核函数中，随后每隔一秒便遍历my_map中的元素，并且打印已分配时间大于1秒的数据对象的信息：对象的首地址,已分配时间以及在哪里被分配