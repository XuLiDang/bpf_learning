#include <linux/ptrace.h>
#include <linux/version.h>
#include <uapi/linux/bpf.h>
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_tracing.h>

struct pair {
	u64 val;
	u64 ip;
};

// 定义BPF_MAP_TYPE_HASH类型的map
struct {
	__uint(type, BPF_MAP_TYPE_HASH);
	__type(key, long);
	__type(value, struct pair);
	__uint(max_entries, 1000000);
} my_map SEC(".maps");

/**
 * kmem_cache_free - Deallocate an object
 * @cachep: The cache the allocation was from.
 * @objp: The previously allocated object.
 *
 * Free an object which was previously allocated from this
 * cache.
 * 
 * void kmem_cache_free(struct kmem_cache *cachep, void *objp)
 */
SEC("kprobe/kmem_cache_free")
int bpf_prog1(struct pt_regs *ctx)
{
	// 从pt_regs结构体中获取传递给被挂载函数的第二个参数
	long ptr = PT_REGS_PARM2(ctx);
	// 删除my_map中key为ptr的元素
	bpf_map_delete_elem(&my_map, &ptr);
	return 0;
}

SEC("kretprobe/kmem_cache_alloc_node")
int bpf_prog2(struct pt_regs *ctx)
{	
	// 从pt_regs结构体中获取kmem_cache_alloc_node函数要返回给调用函数的值
	long ptr = PT_REGS_RC(ctx);
	long ip = 0;

	/* get ip address of kmem_cache_alloc_node() caller */
	// 从pt_regs结构体中获取ip寄存器的值，即kmem_cache_alloc_node函数的返回地址
	BPF_KRETPROBE_READ_RET_IP(ip, ctx);

	struct pair v = {
		// 获取自机器运行到现在的时间长度
		.val = bpf_ktime_get_ns(),
		.ip = ip,
	};

	// 将my_map中key为ptr的元素的值更新为v的值
	bpf_map_update_elem(&my_map, &ptr, &v, BPF_ANY);
	return 0;
}
char _license[] SEC("license") = "GPL";
u32 _version SEC("version") = LINUX_VERSION_CODE;
