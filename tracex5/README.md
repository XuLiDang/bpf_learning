#### 程序功能解析：
##### tracex5_kern.c：
- `tracex5_kern.c`文件中定义了多个bpf程序，一个是通过kprobe挂载到`__seccomp_filter`函数的bpf_prog1程序，而其他的都是段名为"kprobe/SYS__NR_xxx"的bpf程序，此类程序并没有直接挂载到内核函数中，而是通过`bpf_tail_call`函数来调用。

- 这里主要介绍一下bpf_prog1，每当用户程序调用系统调用时，首先需要通过`__seccomp_filter`函数的处理。而运行`__seccomp_filter`函数时，bpf_prog1便会被触发，其会从pt_regs结构体中获取被挂载函数的第一个参数，该参数为系统调用号。随后通过`bpf_tail_call`函数跳转到下一个bpf程序，该函数的第一个参数是上下文；第二个参数则是BPF_MAP_TYPE_PROG_ARRAY类型的map(这里则是progs)；而第三个参数则是progs中的key，即下一个bpf程序在progs中的索引。

##### tracex5_user.c：
- `tracex5_user.c`文件中的主体代码逻辑则是：首先加载bpf目标文件，同时提取bpf程序段名中krpobe之后的字符串并转化成对应的系统调用号，如对于SEC(kprobe/"SYS__NR_write)，提取出来的系统调用号便是SYS__NR_write的值。随后将系统调用号作为key，对应的bpf描述符作为value来更新BPF_MAP_TYPE_PROG_ARRAY类型的map —— progs，`tracex5_kern.c`文件中的尾调用函数需要根据progs的内容来调用下一个bpf程序。

- 接下来便通过`install_accept_all_seccomp`函数来安装seccopm来限制应用程序可调用的系统调用，不过这里允许用户程序调用所有的系统调用，另外值得注意的是：安装seccopm后,用户程序调用系统调用前均需通过`__seccomp_filter`函数的处理。最后运行dd命令产生大量的read和write系统调用，随后调用`read_trace_pipe`函数循环读取并打印`/sys/kernel/debug/tracing/trace_pipe`文件中的内容