#include <linux/ptrace.h>
#include <linux/version.h>
#include <uapi/linux/bpf.h>
#include <uapi/linux/seccomp.h>
#include <uapi/linux/unistd.h>
#include "syscall_nrs.h"
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_tracing.h>

// __stringify(x)的作用是把参数x转换成一个字符串
#define PROG(F) SEC("kprobe/"__stringify(F)) int bpf_func_##F

// 定义BPF_MAP_TYPE_PROG_ARRAY类型的map
struct {
	__uint(type, BPF_MAP_TYPE_PROG_ARRAY);
	__uint(key_size, sizeof(u32));
	__uint(value_size, sizeof(u32));
#ifdef __mips__
	__uint(max_entries, 6000); /* MIPS n64 syscalls start at 5000 */
#else
	__uint(max_entries, 1024);
#endif
} progs SEC(".maps");

// static int __seccomp_filter(int this_syscall, const struct seccomp_data *sd,
// 			    const bool recheck_after_trace)
SEC("kprobe/__seccomp_filter")
int bpf_prog1(struct pt_regs *ctx)
{	
	// 从pt_regs结构体中获取被挂载函数的第一个参数,该参数为系统调用号
	int sc_nr = (int)PT_REGS_PARM1(ctx);

	// long bpf_tail_call(void *ctx, struct bpf_map *prog_array_map, u32 index)
	// sc_nr是BPF_MAP_TYPE_PROG_ARRAY类型的map的索引,即此种类型的map的key
	// 跳转到下一个bpf程序
	bpf_tail_call(ctx, &progs, sc_nr);

	/* fall through -> unknown syscall */
	if (sc_nr >= __NR_getuid && sc_nr <= __NR_getsid) {
		char fmt[] = "syscall=%d (one of get/set uid/pid/gid)\n";
		bpf_trace_printk(fmt, sizeof(fmt), sc_nr);
	}
	return 0;
}

/* we jump here when syscall number == __NR_write */
// SEC("kprobe/"SYS__NR_write) int bpf_func_##SYS__NR_write(struct pt_regs *ctx)
PROG(SYS__NR_write)(struct pt_regs *ctx)
{
	struct seccomp_data sd;

	// 从pt_regs结构体中获取被挂载函数的第二个参数，并从该参数指向的地址处开始
	// 复制sizeof(sd)个字节的数据到&sd指向的地址处
	bpf_probe_read_kernel(&sd, sizeof(sd), (void *)PT_REGS_PARM2(ctx));
	// sd.args[0],[1]和[2]分别代表的是用户程序传入给write系统调用的参数
	if (sd.args[2] == 512) {
		char fmt[] = "write(fd=%d, buf=%p, size=%d)\n";
		bpf_trace_printk(fmt, sizeof(fmt), sd.args[0], sd.args[1], sd.args[2]);
	}
	return 0;
}

// SEC("kprobe/"SYS__NR_read) int bpf_func_##SYS__NR_read(struct pt_regs *ctx)
PROG(SYS__NR_read)(struct pt_regs *ctx)
{
	struct seccomp_data sd;

	// 从pt_regs结构体中获取被挂载函数的第二个参数，并从该参数指向的地址处开始
	// 复制sizeof(sd)个字节的数据到&sd指向的地址处
	bpf_probe_read_kernel(&sd, sizeof(sd), (void *)PT_REGS_PARM2(ctx));
	// sd.args[0],[1]和[2]分别代表的是用户程序传入给read系统调用的参数
	if (sd.args[2] > 128 && sd.args[2] <= 1024) {
		char fmt[] = "read(fd=%d, buf=%p, size=%d)\n";
		bpf_trace_printk(fmt, sizeof(fmt),
				 sd.args[0], sd.args[1], sd.args[2]);
	}
	return 0;
}

#ifdef __NR_mmap2
// SEC("kprobe/"SYS__NR_mmap2) int bpf_func_##SYS__NR_mmap2(struct pt_regs *ctx)
PROG(SYS__NR_mmap2)(struct pt_regs *ctx)
{
	char fmt[] = "mmap2\n";

	bpf_trace_printk(fmt, sizeof(fmt));
	return 0;
}
#endif

#ifdef __NR_mmap
// SEC("kprobe/"SYS__NR_mmap) int bpf_func_##SYS__NR_mmap(struct pt_regs *ctx)
PROG(SYS__NR_mmap)(struct pt_regs *ctx)
{
	char fmt[] = "mmap\n";

	bpf_trace_printk(fmt, sizeof(fmt));
	return 0;
}
#endif

char _license[] SEC("license") = "GPL";
u32 _version SEC("version") = LINUX_VERSION_CODE;
