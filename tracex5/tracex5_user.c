// SPDX-License-Identifier: GPL-2.0
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/filter.h>
#include <linux/seccomp.h>
#include <sys/prctl.h>
#include <bpf/bpf.h>
#include <bpf/libbpf.h>
#include <sys/resource.h>
#include "trace_helpers.h"

#ifdef __mips__
#define	MAX_ENTRIES  6000 /* MIPS n64 syscalls start at 5000 */
#else
#define	MAX_ENTRIES  1024
#endif

/* install fake seccomp program to enable seccomp code path inside the kernel,
 * so that our kprobe attached to seccomp_phase1() can be triggered
 */
// 有关seccomp的介绍可参考: https://bbs.kanxue.com/thread-273495.htm
static void install_accept_all_seccomp(void)
{	
	// BPF过滤指令,BPF_STMT与BPF_JUMP宏均定义在linux/filter.h头文件中,用来定义BPF过滤指令
	struct sock_filter filter[] = {
		// BPF_STMT有两个参数，操作码(code)和值(k)
		// 表示程序终止,BPF_RET表示程序返回。返回值为SECCOMP_RET_ALLOW,表示允许系统调用
		BPF_STMT(BPF_RET+BPF_K, SECCOMP_RET_ALLOW),
	};

	// 定义sock_fprog结构体,该结构体包含指向filter数组的指针以及数组中BPF过滤指令的数量
	struct sock_fprog prog = {
		.len = (unsigned short)(sizeof(filter)/sizeof(filter[0])),
		.filter = filter,
	};

	// PR_SET_SECCOMP表示为进程设置seccomp,而SECCOMP_MODE_FILTER则指定seccomp的模式
  	// 最后的&prog则是指向sock_fprog结构体的指针,该行语句的作用便是加载过滤器
	if (prctl(PR_SET_SECCOMP, 2, &prog))
		perror("prctl");
}

int main(int ac, char **argv)
{
	struct bpf_link *link = NULL;
	struct bpf_program *prog;
	struct bpf_object *obj;
	int key, fd, progs_fd;
	const char *section;
	char filename[256];
	FILE *f;

	snprintf(filename, sizeof(filename), "%s_kern.o", argv[0]);
	obj = bpf_object__open_file(filename, NULL);
	if (libbpf_get_error(obj)) {
		fprintf(stderr, "ERROR: opening BPF object file failed\n");
		return 0;
	}

	prog = bpf_object__find_program_by_name(obj, "bpf_prog1");
	if (!prog) {
		printf("finding a prog in obj file failed\n");
		goto cleanup;
	}

	/* load BPF program */
	if (bpf_object__load(obj)) {
		fprintf(stderr, "ERROR: loading BPF object file failed\n");
		goto cleanup;
	}

	// 将bpf_prog1函数挂载到对应的内核函数中
	link = bpf_program__attach(prog);
	if (libbpf_get_error(link)) {
		fprintf(stderr, "ERROR: bpf_program__attach failed\n");
		link = NULL;
		goto cleanup;
	}

	// 获取map的文件描述符
	progs_fd = bpf_object__find_map_fd_by_name(obj, "progs");
	if (progs_fd < 0) {
		fprintf(stderr, "ERROR: finding a map in obj file failed\n");
		goto cleanup;
	}

	// 遍历bpf目标文件中所有的bpf程序
	bpf_object__for_each_program(prog, obj) {
		// 获得段名,即用SEC所定义的名称,这里的格式是"kprobe/xxxx"
		section = bpf_program__section_name(prog);
		// 将段名中的系统调用号赋值给key,如在kprobe/SYS__NR_write中
		// 系统调用号则是SYS__NR_write
		if (sscanf(section, "kprobe/%d", &key) != 1)
			continue;
		// 获取bpf程序的描述符
		fd = bpf_program__fd(prog);
		// 更新文件描述符为progs_fd的map中的元素
		// 元素的key为系统调用号,对应的value为bpf程序的描述符
		bpf_map_update_elem(progs_fd, &key, &fd, BPF_ANY);
	}

	// 安装seccopm来限制应用程序可调用的系统调用,不过这里允许调用所有的系统调用
	// 安装seccopm后,调用系统调用前均需通过__seccomp_filter函数的处理
	install_accept_all_seccomp();

	// 产生大量的read和write系统调用
	f = popen("dd if=/dev/zero of=/dev/null count=5", "r");
	(void) f;

	// 循环读取并打印/sys/kernel/debug/tracing/trace_pipe文件中的内容
	read_trace_pipe();

cleanup:
	bpf_link__destroy(link);
	bpf_object__close(obj);
	return 0;
}
