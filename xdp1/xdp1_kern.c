/* Copyright (c) 2016 PLUMgrid
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 */
#define KBUILD_MODNAME "foo"
#include <uapi/linux/bpf.h>
#include <linux/in.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <linux/if_vlan.h>
#include <linux/ip.h>
#include <linux/ipv6.h>
#include <bpf/bpf_helpers.h>

// 定义BPF_MAP_TYPE_PERCPU_ARRAY类型的map
// SEC(".maps")的作用是: 在该源代码文件编译后生成的目标文件中生成一个名为".maps"的段(section)
struct {
	__uint(type, BPF_MAP_TYPE_PERCPU_ARRAY);
	__type(key, u32);
	__type(value, long);
	__uint(max_entries, 256);
} rxcnt SEC(".maps");

// 解析IPV4数据包
static int parse_ipv4(void *data, u64 nh_off, void *data_end)
{
	struct iphdr *iph = data + nh_off;

	if (iph + 1 > data_end)
		return 0;
	// 返回IPV4数据包头部的协议类型值
	return iph->protocol;
}

// 解析IPV6数据包
static int parse_ipv6(void *data, u64 nh_off, void *data_end)
{
	struct ipv6hdr *ip6h = data + nh_off;

	if (ip6h + 1 > data_end)
		return 0;
	// 返回IPV6数据包头部的协议类型值
	return ip6h->nexthdr;
}

// SEC("xdp1")的作用是: 在该源代码文件编译后生成的目标文件中生成一个名为".xdp1"的段(section)
SEC("xdp1")
int xdp_prog1(struct xdp_md *ctx)
{
	// 指向xdp_md数据区的结束地址
	void *data_end = (void *)(long)ctx->data_end;
	// 指向xdp_md数据区的起始地址
	void *data = (void *)(long)ctx->data;
	// 将data赋值给指向ethhdr结构体的指针,该结构体用于存放以太网帧头部的信息
	struct ethhdr *eth = data;
	// 默认值为丢弃所接收到的数据包
	int rc = XDP_DROP;
	// map的值
	long *value;
	// 以太网帧以及VLAN数据包头部的协议字段值
	u16 h_proto;
	// 当前偏移量
	u64 nh_off;
	// ip协议
	u32 ipproto;

	// 将nh_off的值更新为ethhdr结构体的长度值
	nh_off = sizeof(*eth);
	if (data + nh_off > data_end)
		return rc;
	// 获得以太网帧头部中的协议字段值
	h_proto = eth->h_proto;

	/* Handle VLAN tagged packet */
	if (h_proto == htons(ETH_P_8021Q) || h_proto == htons(ETH_P_8021AD)) {
		// VLAN数据包头结构体
		struct vlan_hdr *vhdr;
		// xdp_md数据区的起始地址 + ethhdr结构体的长度值
		vhdr = data + nh_off;
		// 将nh_off的值更新为:ethhdr结构体的长度值 + vlan_hdr结构体长度值
		nh_off += sizeof(struct vlan_hdr);
		if (data + nh_off > data_end)
			return rc;
		// 获取VLAN数据包头结构体中的协议字段值
		h_proto = vhdr->h_vlan_encapsulated_proto;
	}
	/* Handle double VLAN tagged packet */
	if (h_proto == htons(ETH_P_8021Q) || h_proto == htons(ETH_P_8021AD)) {
		// VLAN数据包头结构体
		struct vlan_hdr *vhdr;
		// xdp_md数据区的起始地址 + ethhdr结构体的长度值 + vlan_hdr结构体长度值
		vhdr = data + nh_off;
		// 将nh_off的值更新为：ethhdr结构体的长度值 + vlan_hdr结构体长度值 + vlan_hdr结构体长度值
		nh_off += sizeof(struct vlan_hdr);
		if (data + nh_off > data_end)
			return rc;
		// 获取VLAN数据包头结构体中的协议字段值
		h_proto = vhdr->h_vlan_encapsulated_proto;
	}

	// 处理IPV4数据包
	if (h_proto == htons(ETH_P_IP))
		ipproto = parse_ipv4(data, nh_off, data_end);
	// 处理IPV6数据包
	else if (h_proto == htons(ETH_P_IPV6))
		ipproto = parse_ipv6(data, nh_off, data_end);
	else
		ipproto = 0;
	// ip协议值为key, 查找对应的元素并返回value
	value = bpf_map_lookup_elem(&rxcnt, &ipproto);
	if (value)
		*value += 1;

	return rc;
}

char _license[] SEC("license") = "GPL";
