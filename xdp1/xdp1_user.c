// SPDX-License-Identifier: GPL-2.0-only
/* Copyright (c) 2016 PLUMgrid
 */
#include <linux/bpf.h>
#include <linux/if_link.h>
#include <assert.h>
#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libgen.h>
#include <sys/resource.h>
#include <net/if.h>

#include "bpf_util.h"
#include <bpf/bpf.h>
#include <bpf/libbpf.h>

static int ifindex;
static __u32 xdp_flags = XDP_FLAGS_UPDATE_IF_NOEXIST;
static __u32 prog_id;

static void int_exit(int sig)
{
	__u32 curr_prog_id = 0;

	// 获得挂载到网卡驱动中XDP driver hook的bpf程序id
	if (bpf_get_link_xdp_id(ifindex, &curr_prog_id, xdp_flags)) {
		printf("bpf_get_link_xdp_id failed\n");
		exit(1);
	}
	if (prog_id == curr_prog_id)
		// 解除挂载
		bpf_set_link_xdp_fd(ifindex, -1, xdp_flags);
	else if (!curr_prog_id)
		printf("couldn't find a prog id on a given interface\n");
	else
		printf("program on interface changed, not removing\n");
	exit(0);
}

/* simple per-protocol drop counter
 */
static void poll_stats(int map_fd, int interval)
{	
	// 获取系统中可用的CPU数
	unsigned int nr_cpus = bpf_num_possible_cpus();
	__u64 values[nr_cpus], prev[UINT8_MAX] = { 0 };
	int i;

	// 每隔两秒运行一次
	while (1) {
		__u32 key = UINT32_MAX;

		sleep(interval);
		// 遍历map中的每个元素
		while (bpf_map_get_next_key(map_fd, &key, &key) != -1) {
			__u64 sum = 0;
			// 查到对应的元素的value
			// map_fd代表的map的类型是BPF_MAP_TYPE_PERCPU_ARRAY
			// 这就意味着，每个CPU都有自己独立的映射，因此要将每个map中元素的值累加
			assert(bpf_map_lookup_elem(map_fd, &key, values) == 0);
			for (i = 0; i < nr_cpus; i++)
				sum += values[i];
			if (sum > prev[key])
				printf("proto %u: %10llu pkt/s\n",
				       key, (sum - prev[key]) / interval);
			prev[key] = sum;
		}
	}
}

static void usage(const char *prog)
{
	fprintf(stderr,
		"usage: %s [OPTS] IFACE\n\n"
		"OPTS:\n"
		"    -S    use skb-mode\n"
		"    -N    enforce native mode\n"
		"    -F    force loading prog\n",
		prog);
}

int main(int argc, char **argv)
{	
	// struct bpf_prog_load_attr {
	// 	const char *file;
	// 	enum bpf_prog_type prog_type;
	// 	enum bpf_attach_type expected_attach_type;
	// 	int ifindex;
	// 	int log_level;
	// 	int prog_flags;
	// };
	struct bpf_prog_load_attr prog_load_attr = {
		.prog_type	= BPF_PROG_TYPE_XDP,
	};
	struct bpf_prog_info info = {};
	__u32 info_len = sizeof(info);
	const char *optstr = "FSN";
	int prog_fd, map_fd, opt;
	struct bpf_object *obj;
	struct bpf_map *map;
	char filename[256];
	int err;

	// getopt() 方法是用来分析命令行参数的，该方法由 Unix 标准库提供，包含在 <unistd.h> 头文件中
	while ((opt = getopt(argc, argv, optstr)) != -1) {
		switch (opt) {
		case 'S':
			// 增加XDP_FLAGS_SKB_MODE标志位
			xdp_flags |= XDP_FLAGS_SKB_MODE;
			break;
		case 'N':
			/* default, set below */
			break;
		case 'F':
			// 去除掉XDP_FLAGS_UPDATE_IF_NOEXIST标志位
			xdp_flags &= ~XDP_FLAGS_UPDATE_IF_NOEXIST;
			break;
		default:
			usage(basename(argv[0]));
			return 1;
		}
	}

	// xdp_flags中没有XDP_FLAGS_SKB_MODE标志位
	if (!(xdp_flags & XDP_FLAGS_SKB_MODE))
		// 增加XDP_FLAGS_DRV_MODE标志位
		xdp_flags |= XDP_FLAGS_DRV_MODE;

	if (optind == argc) {
		usage(basename(argv[0]));
		return 1;
	}

	// if_nametoindex()：指定网络接口名称字符串作为参数；若该接口存在，则返回相应的索引，否则返回0
	ifindex = if_nametoindex(argv[optind]);
	if (!ifindex) {
		perror("if_nametoindex");
		return 1;
	}

	snprintf(filename, sizeof(filename), "%s_kern.o", argv[0]);
	prog_load_attr.file = filename;

	// 加载bpf目标文件，并返回指向bpf_object结构体的指针和bpf程序描述符
	// 此时prog_load_attr指向的结构体中只有file和prog_type字段是有效的
	if (bpf_prog_load_xattr(&prog_load_attr, &obj, &prog_fd))
		return 1;
	// 获取bpf目标文件中定义的map
	map = bpf_map__next(NULL, obj);
	if (!map) {
		printf("finding a map in obj file failed\n");
		return 1;
	}
	// 获取map的文件描述符
	map_fd = bpf_map__fd(map);

	if (!prog_fd) {
		printf("bpf_prog_load_xattr: %s\n", strerror(errno));
		return 1;
	}
	// (Signal Interrupt) 中断信号，由int_exit函数处理
	signal(SIGINT, int_exit);
	// (Signal Terminate) 发送给本程序的终止请求信号，由int_exit函数处理
	signal(SIGTERM, int_exit);

	// 将bpf程序挂载到网卡驱动中的XDP driver hook，那么每当网卡收到一个packet
	// 该bpf程序都会执行一次
	if (bpf_set_link_xdp_fd(ifindex, prog_fd, xdp_flags) < 0) {
		printf("link set xdp fd failed\n");
		return 1;
	}
	// 获得bpf程序的信息
	err = bpf_obj_get_info_by_fd(prog_fd, &info, &info_len);
	if (err) {
		printf("can't get prog info - %s\n", strerror(errno));
		return err;
	}
	prog_id = info.id;

	poll_stats(map_fd, 2);

	return 0;
}
