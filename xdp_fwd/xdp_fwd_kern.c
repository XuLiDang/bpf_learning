// SPDX-License-Identifier: GPL-2.0
/* Copyright (c) 2017-18 David Ahern <dsahern@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 */
#define KBUILD_MODNAME "foo"
#include <uapi/linux/bpf.h>
#include <linux/in.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <linux/if_vlan.h>
#include <linux/ip.h>
#include <linux/ipv6.h>

#include <bpf/bpf_helpers.h>

#define IPV6_FLOWINFO_MASK              cpu_to_be32(0x0FFFFFFF)

// This specialized type of map stores references to network devices.
// You can build a virtual map of ports that point to specific network 
// devices and then redirect packets by using the helper bpf_redirect_map.
struct {
	__uint(type, BPF_MAP_TYPE_DEVMAP);
	__uint(key_size, sizeof(int));
	__uint(value_size, sizeof(int));
	__uint(max_entries, 64);
} xdp_tx_ports SEC(".maps");

/* from include/net/ip.h */
static __always_inline int ip_decrease_ttl(struct iphdr *iph)
{
	u32 check = (__force u32)iph->check;

	check += (__force u32)htons(0x0100);
	iph->check = (__force __sum16)(check + (check >= 0xFFFF));
	return --iph->ttl;
}

static __always_inline int xdp_fwd_flags(struct xdp_md *ctx, u32 flags)
{	
	// 指向xdp_md数据区的结束地址
	void *data_end = (void *)(long)ctx->data_end;
	// 指向xdp_md数据区的起始地址
	void *data = (void *)(long)ctx->data;
	struct bpf_fib_lookup fib_params;
	// 将data赋值给指向ethhdr结构体的指针,该结构体用于存放以太网帧头部的信息
	struct ethhdr *eth = data;
	// IPV6头部结构体
	struct ipv6hdr *ip6h;
	// IPV4头部结构体
	struct iphdr *iph;
	u16 h_proto;
	// 当前偏移量
	u64 nh_off;
	int rc;

	// 将nh_off的值更新为ethhdr结构体的长度值
	nh_off = sizeof(*eth);
	if (data + nh_off > data_end)
		// 返回XDP_DROP代表直接丢弃所接收到的数据包
		return XDP_DROP;
	// GCC提供了一系列的builtin函数，可以实现一些简单快捷的功能来方便程序编写。
	// builtin函数可用来优化编译结果。这些函数以 "__builtin_" 作为函数名前缀。
	__builtin_memset(&fib_params, 0, sizeof(fib_params));

	// 获取以太网帧头部结构体中的协议字段
	h_proto = eth->h_proto;
	// 以太网帧头部结构体中的协议字段为ETH_P_IP
	if (h_proto == htons(ETH_P_IP)) {
		// iph指向的地址为：xdp_md数据区的起始地址 + ethhdr结构体的长度值
		iph = data + nh_off;
		// 若iph指向的是xdp_md数据区的结束地址则直接返回XDP_DROP
		if (iph + 1 > data_end)
			// 返回XDP_DROP代表直接丢弃所接收到的数据包
			return XDP_DROP;
		// IPV4头部结构体中的ttl属性小于等于1
		if (iph->ttl <= 1)
			// 返回XDP_PASS代表要将数据包交由网络协议栈处理
			return XDP_PASS;

		// AF_INET（TCP/IP – IPv4）
		fib_params.family	= AF_INET;
		fib_params.tos		= iph->tos;
		// IPV4头结构体中的协议字段
		fib_params.l4_protocol	= iph->protocol;
		// 源端口
		fib_params.sport	= 0;
		// 目的端口
		fib_params.dport	= 0;
		fib_params.tot_len	= ntohs(iph->tot_len);
		// 源地址
		fib_params.ipv4_src	= iph->saddr;
		// 目的地址
		fib_params.ipv4_dst	= iph->daddr;
	}
	// 以太网帧头部结构体中的协议字段为ETH_P_IPV6
	else if (h_proto == htons(ETH_P_IPV6)) {
		// IPV6数据包源地址
		struct in6_addr *src = (struct in6_addr *) fib_params.ipv6_src;
		// IPV6数据包目的地址
		struct in6_addr *dst = (struct in6_addr *) fib_params.ipv6_dst;
		// ip6h指向的地址为：xdp_md数据区的起始地址 + ethhdr结构体的长度值
		ip6h = data + nh_off;
		// 若ip6h指向的是xdp_md数据区的结束地址则直接返回XDP_DROP
		if (ip6h + 1 > data_end)
			return XDP_DROP;
		// IPV6头结构体中的hop_limit字段值小于等于1
		if (ip6h->hop_limit <= 1)
			return XDP_PASS;

		// AF_INET6（TCP/IP – IPv6）
		fib_params.family	= AF_INET6;
		fib_params.flowinfo	= *(__be32 *)ip6h & IPV6_FLOWINFO_MASK;
		// IPV6头结构体中的协议字段
		fib_params.l4_protocol	= ip6h->nexthdr;
		// 源端口
		fib_params.sport	= 0;
		// 目的端口
		fib_params.dport	= 0;
		fib_params.tot_len	= ntohs(ip6h->payload_len);
		// 源地址
		*src			= ip6h->saddr;
		// 目的地址
		*dst			= ip6h->daddr;
	} else {
		// 返回XDP_PASS代表要将数据包交由网络协议栈处理
		return XDP_PASS;
	}
	// 获取接收当前数据包的网络接口索引
	fib_params.ifindex = ctx->ingress_ifindex;

	// 查找内核中的路由表，判断当前数据包能否重定向
	rc = bpf_fib_lookup(ctx, &fib_params, sizeof(fib_params), flags);
	/*
	 * Some rc (return codes) from bpf_fib_lookup() are important,
	 * to understand how this XDP-prog interacts with network stack.
	 *
	 * BPF_FIB_LKUP_RET_NO_NEIGH:
	 *  Even if route lookup was a success, then the MAC-addresses are also
	 *  needed.  This is obtained from arp/neighbour table, but if table is
	 *  (still) empty then BPF_FIB_LKUP_RET_NO_NEIGH is returned.  To avoid
	 *  doing ARP lookup directly from XDP, then send packet to normal
	 *  network stack via XDP_PASS and expect it will do ARP resolution.
	 *
	 * BPF_FIB_LKUP_RET_FWD_DISABLED:
	 *  The bpf_fib_lookup respect sysctl net.ipv{4,6}.conf.all.forwarding
	 *  setting, and will return BPF_FIB_LKUP_RET_FWD_DISABLED if not
	 *  enabled this on ingress device.
	 */
	// 返回值为SUCCESS则代表可以对数据包进行重定向，并且fib_params结构体中的
	// "目的IP地址会被修改为下一跳的目的IP地址，源MAC地址"会被修改为出口设备的
	// MAC地址，而目的MAC地址则会被修改为下一跳的MAC地址，ifindex则会被修改为
	// 下一跳的网络接口设备索引
	if (rc == BPF_FIB_LKUP_RET_SUCCESS) {
		/* Verify egress index has been configured as TX-port.
		 * (Note: User can still have inserted an egress ifindex that
		 * doesn't support XDP xmit, which will result in packet drops).
		 *
		 * Note: lookup in devmap supported since 0cdbb4b09a0.
		 * If not supported will fail with:
		 *  cannot pass map_type 14 into func bpf_map_lookup_elem#1:
		 */
		// 此时fib_params.ifindex已被修改为下一跳的网络接口设备索引
		// 函数的返回值为0则说明该索引对应的接口已挂载了XDP程序
		if (!bpf_map_lookup_elem(&xdp_tx_ports, &fib_params.ifindex))
			return XDP_PASS;

		if (h_proto == htons(ETH_P_IP))
			// 减少IPV4数据包头部中的TTL字段值
			ip_decrease_ttl(iph);
		else if (h_proto == htons(ETH_P_IPV6))
			// 减少IPV6数据包头部中的hop_limit字段值
			ip6h->hop_limit--;

		// 将fib_params.dmac复制给eth->h_dest，更新以太网帧中的源MAC地址
		memcpy(eth->h_dest, fib_params.dmac, ETH_ALEN);
		// 将fib_params.dmac复制给eth->h_dest，更新以太网帧中的目的MAC地址
		memcpy(eth->h_source, fib_params.smac, ETH_ALEN);
		// Redirect the packet to another net device of index ifindex
		return bpf_redirect_map(&xdp_tx_ports, fib_params.ifindex, 0);
	}

	return XDP_PASS;
}

// SEC("xdp_fwd")的作用是: 在该源代码文件编译后生成的目标文件中生成一个名为".xdp_fwd"的段(section)
SEC("xdp_fwd")
int xdp_fwd_prog(struct xdp_md *ctx)
{
	return xdp_fwd_flags(ctx, 0);
}

// SEC("xdp_fwd_direct")的作用是: 在该源代码文件编译后生成的目标文件中生成一个名为".xdp_fwd_direct"的段(section)
SEC("xdp_fwd_direct")
int xdp_fwd_direct_prog(struct xdp_md *ctx)
{
	return xdp_fwd_flags(ctx, BPF_FIB_LOOKUP_DIRECT);
}

char _license[] SEC("license") = "GPL";
